> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Rhianna Reichert

### Project 1 Requirements:

*Three Parts:*

1. Create .mwb file
2. Locally create 11 tables using MySQL
    - a.) seven under "Assignment" layer
    - b.) four under "Court" layer
3. Chapter Questions
    - Chapter 13

#### README.md file should include the following items:

* Screenshot of MySQL code
* Screenshot of populated ERD
* Bitbucket repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Project Screenshots:

*Screenshot of P1 ERD*

|        P1 Assignment Layer        |       P1 Court Layer   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![ P1 Assignment Layer](img/assignment_layer_ERD.png " P1 Assignment Layer")              |                    ![P1 Court Layer](img/court_layer_ERD.png "P1 Court Layer")


*Screenshot of P1 Populated Tables*

|        P1 Phone Table        |       P1 Person Table   |       P1 Bar Table   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![P1 Phone Table](img/phone_table.png "P1 Phone Table")              |                    ![P1 Person Table](img/person_table.png "P1 Person Table")|                    ![P1 Bar Table](img/bar_table.png "P1 Bar Table")


|        P1 Speciality Table        |       P1 Attorney Table   |       P1 Client Table   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![P1 Speciality Table](img/speciality_table.png "P1 Speciality Table")              |                    ![P1 Attorney Table](img/attorney_table.png "P1 Attorney Table")|                    ![P1 Client Table](img/client_table.png "P1 Client Table")


|        P1 Assignment Table        |       P1 Judge Table   |       P1 Court Table   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![P1 Assignment Table](img/assignment_table.png "P1 Assignment Table")              |                    ![P1 Judge Table](img/judge_table.png "P1 Judge Table")|                    ![P1 Court Table](img/court_table.png "P1 Court Table")


|        P1 Case Table        |       P1 Judge_Hist Table   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![P1 Case Table](img/case_table.png "P1 Case Table")              |                    ![P1 Judge_Hist Table](img/judge_hist_table.png "P1 Judge_Hist Table")


*GIFS of P1 Report Outcomes and Code*

|        P1 ALL Tables        |          P1 Solutions Code       |        P1 Report Outcomes        |                                     
|:---------------------------------:|:---------------------------------:|:---------------------------------:|
|![P1 ALL Tables](img/ERD_tables.gif "P1 ALL Tables")                 |              ![P1 Solutions Code](img/solutions_code.gif "P1 Solutions Code")                 |              ![P1 Report Outcomes](img/report_outcomes.gif "P1 Report Outcomes")                 


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
