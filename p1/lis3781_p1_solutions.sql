/*
Svae the following code as lis3781_p1_solutions.sql

-- create/populate database and tables...
-- \. F:\Courses\_LIS3781\project1\lis3781_p1_solutions.sql
*/

-- ################## CREATE DATABASE AND TABLES ##################

-- see data output after insert statements below
select 'drop, create, use database, create tables, display data:' as '';
do sleep(5);    --pause MySql console output

DROP SCHEMA IF EXISTS rnr06;
CREATE SCHEMA IF NOT EXISTS rnr06;
SHOW WARNINGS;
USE rnr06;

-- -------------------------------------
-- Table person
-- -------------------------------------
-- NOTE: allow per_ssn to ne null, in order to use stored proc CreatePersonSSN below
DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person
(
    per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_ssn BINARY(64) NULL,
    per_safe binary(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL,
    per_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
    per_email VARCHAR(100) NOT NULL,
    per_dob DATE NOT NULL,
    per_type ENUM('a','c','j') NOT NULL,
    per_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),
    UNIQUE INDEX ux_per_ssn (per_ssn ASC)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
SHOW WARNINGS;

-- -------------------------------------
-- Table attorney
-- -------------------------------------
DROP TABLE IF EXISTS attorney;
CREATE TABLE IF NOT EXISTS attorney
(
    per_id SMALLINT UNSIGNED NOT NULL,
    aty_start_date DATE NOT NULL,
    aty_end_date DATE NULL DEFAULT NULL,
    aty_hourly_rate DECIMAL(5,2) UNSIGNED NOT NULL,
    aty_years_in_practice TINYINT NOT NULL,
    aty_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_attorney_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -------------------------------------
-- Table client
-- -------------------------------------
DROP TABLE IF EXISTS client;
CREATE TABLE IF NOT EXISTS client
(
    per_id SMALLINT UNSIGNED NOT NULL,
    cli_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_client_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -------------------------------------
-- Table court
-- -------------------------------------
DROP TABLE IF EXISTS court;
CREATE TABLE IF NOT EXISTS court
(
    crt_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    crt_name VARCHAR(45) NOT NULL,
    crt_street VARCHAR(30) NOT NULL,
    crt_city VARCHAR(30) NOT NULL,
    crt_state CHAR(2) NOT NULL,
    crt_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
    crt_phone BIGINT NOT NULL,
    crt_email VARCHAR(100) NOT NULL,
    crt_url VARCHAR(100) NOT NULL,
    crt_notes VARCHAR(255) NULL,
    PRIMARY KEY (crt_id)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -------------------------------------
-- Table judge
-- -------------------------------------
DROP TABLE IF EXISTS judge;
CREATE TABLE IF NOT EXISTS judge
(
    per_id SMALLINT UNSIGNED NOT NULL,
    crt_id TINYINT UNSIGNED NULL DEFAULT NULL,
    jud_salary DECIMAL(8,2) NOT NULL,
    jud_years_in_practice TINYINT UNSIGNED NOT NULL,
    jud_notes VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (per_id),

    INDEX idx_per_id (per_id ASC),
    INDEX idx_crt_id (crt_id ASC),
    
    CONSTRAINT fk_judge_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,
    
    CONSTRAINT fk_judge_court
        FOREIGN KEY (crt_id)
        REFERENCES court (crt_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -------------------------------------
-- Table judge_hist
-- -------------------------------------
DROP TABLE IF EXISTS judge_hist;
CREATE TABLE IF NOT EXISTS judge_hist
(
    jhs_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    jhs_crt_id TINYINT NULL,
    jhs_date timestamp NOT NULL default current_timestamp(),
    jhs_type enum('i','u','d') NOT NULL default 'i',
    jhs_salary DECIMAL(8,2) NOT NULL,
    jhs_notes VARCHAR(255) NULL,
    PRIMARY KEY (jhs_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_judge_hist_judge
        FOREIGN KEY (per_id)
        REFERENCES judge (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)

ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -------------------------------------
-- Table `case`
-- -------------------------------------
DROP TABLE IF EXISTS `case`;
CREATE TABLE IF NOT EXISTS `case`
(
    cse_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    cse_type VARCHAR(45) NOT NULL,
    cse_description TEXT NOT NULL,
    cse_start_date DATE NOT NULL,
    cse_end_date DATE NULL,
    cse_notes VARCHAR(255) NULL,
    PRIMARY KEY (cse_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_court_case_judge
        FOREIGN KEY (per_id)
        REFERENCES judge (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)

ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -------------------------------------
-- Table bar
-- -------------------------------------
DROP TABLE IF EXISTS bar;
CREATE TABLE IF NOT EXISTS bar
(
    bar_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    bar_name VARCHAR(45) NOT NULL,
    bar_notes VARCHAR(255) NULL,
    PRIMARY KEY (bar_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_bar_attorney
        FOREIGN KEY (per_id)
        REFERENCES attorney (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)

ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -------------------------------------
-- Table speciality
-- -------------------------------------
DROP TABLE IF EXISTS speciality;
CREATE TABLE IF NOT EXISTS speciality
(
    spc_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    spc_type VARCHAR(45) NOT NULL,
    spc_notes VARCHAR(255) NULL,
    PRIMARY KEY (spc_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_speciality_attorney
        FOREIGN KEY (per_id)
        REFERENCES attorney (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)

ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -------------------------------------
-- Table assignment
-- -------------------------------------
DROP TABLE IF EXISTS assignment;
CREATE TABLE IF NOT EXISTS assignment
(
    asn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_cid SMALLINT UNSIGNED NOT NULL,
    per_aid SMALLINT UNSIGNED NOT NULL,
    cse_id SMALLINT UNSIGNED NOT NULL,
    asn_notes VARCHAR(255) NULL,
    PRIMARY KEY (asn_id),

    INDEX idx_per_cid (per_cid ASC),
    INDEX idx_per_aid (per_aid ASC),
    INDEX idx_cse_id (cse_id ASC),

    UNIQUE INDEX ux_per_cid_per_aid_cse_id (per_cid ASC, per_aid ASC, cse_id ASC),

    CONSTRAINT fk_assign_case
        FOREIGN KEY (cse_id)
        REFERENCES `case` (cse_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_assignment_client
        FOREIGN KEY (per_cid)
        REFERENCES client (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_assignment_attorney
        FOREIGN KEY (per_aid)
        REFERENCES attorney (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)

ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -------------------------------------
-- Table phone
-- -------------------------------------
DROP TABLE IF EXISTS phone;
CREATE TABLE IF NOT EXISTS phone
(
    phn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    per_id SMALLINT UNSIGNED NOT NULL,
    phn_num BIGINT UNSIGNED NOT NULL,
    phn_type ENUM('h','c','w','f') NOT NULL COMMENT 'home, cell, work, fax',
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    INDEX idx_per_id (per_id ASC),

    CONSTRAINT fk_phone_person
        FOREIGN KEY (per_id)
        REFERENCES person (per_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)

ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- ################## POPULATE TABLES ##################
-- per_id values (must match for SQL statements to work!):
-- person (super type): 1 - 15
    -- client (sub type): 1 - 5
    -- attorney (sub type): 6 - 10
    -- judge (sub type): 11 - 15

-- -------------------------------------
-- Data for table person
-- -------------------------------------
START TRANSACTION;

INSERT INTO person
(per_id, per_ssn, per_safe, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve', 'Rogers', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', '1923-10-03', 'c', NULL),
(NULL, NULL, NULL, 'Bruce', 'Wayne', '1007 Mountain Drive', 'Gotham', 'NY', 003208440, 'bwayne@knology.net', '1968-03-20', 'c', NULL),
(NULL, NULL, NULL, 'Peter', 'Parker', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', '1988-09-12', 'c', NULL),
(NULL, NULL, NULL, 'Jane', 'Thompson', '13563 Ocean View Drive', 'Seattle', 'WA', 032084409, 'jthompson@gmail.com', '1978-05-08', 'c', NULL),
(NULL, NULL, NULL, 'Debra', 'Steele', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', '1994-07-19', 'c', NULL),
(NULL, NULL, NULL, 'Tony', 'Stark', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', '1972-05-04', 'a', NULL),
(NULL, NULL, NULL, 'Hank', 'Pymi', '2355 Brown Street', 'Cleveland', 'OH', 022348890, 'hpym@aol.com', '1980-08-28', 'a', NULL),
(NULL, NULL, NULL, 'Bob', 'Best', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', '1992-02-10', 'a', NULL),
(NULL, NULL, NULL, 'Sandra', 'Dole', '87912 Lawrence Ave', 'Atlanta', 'GA', 002348890, 'sdole@gmail.com', '1990-01-26', 'a', NULL),
(NULL, NULL, NULL, 'Ben', 'Avery', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', '1983-12-24', 'a', NULL),
(NULL, NULL, NULL, 'Arthur', 'Curry', '3304 Euclid Avenue', 'Miami', 'FL', 000219932, 'acurry@gmail.com', '1975-12-15', 'j', NULL),
(NULL, NULL, NULL, 'Diana', 'Price', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', '1980-08-22', 'j', NULL),
(NULL, NULL, NULL, 'Adam', 'Jurris', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', '1995-01-31', 'j', NULL),
(NULL, NULL, NULL, 'Judy', 'Sleen', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@symaptico.com', '1970-03-22', 'j', NULL),
(NULL, NULL, NULL, 'Bill', 'Neiderheim', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', '1982-03-13', 'j', NULL);

COMMIT;

-- -------------------------------------
-- Data for table phone
-- -------------------------------------
START TRANSACTION;

INSERT INTO phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES
(NULL, 1, 8032288827, 'c', NULL),
(NULL, 2, 2052338293, 'h', NULL),
(NULL, 4, 1034325598, 'w', 'has two office numbers'),
(NULL, 5, 6402338494, 'w', NULL),
(NULL, 6, 5508329842, 'f', 'fax number not currently working'),
(NULL, 7, 8202052203, 'c', 'prefers home calls'),
(NULL, 8, 4008338294, 'h', NULL),
(NULL, 9, 7654328912, 'w', NULL),
(NULL, 10, 5463721984, 'f', 'work fax number'),
(NULL, 11, 4537821902, 'h', 'prefers cell phone calls'),
(NULL, 12, 7867821902, 'w', 'best number to reach'),
(NULL, 13, 4537821654, 'w', 'call during lunch'),
(NULL, 14, 3721821902, 'c', 'prefers cell phone calls'),
(NULL, 15, 9217821945, 'f', 'use for faxing legal docs');

COMMIT;

-- -------------------------------------
-- Data for table client (also, could include other client attributes)
-- -------------------------------------
START TRANSACTION;

INSERT INTO client
(per_id, cli_notes)
VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, NULL);

COMMIT;

-- -------------------------------------
-- Data for table attorney
-- -------------------------------------
START TRANSACTION;

INSERT INTO attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
VALUES
(6, '2006-06-12', NULL, 85, 5, NULL),
(7, '2003-08-20', NULL, 130, 28, NULL),
(8, '2009-12-12', NULL, 70, 17, NULL),
(9, '2008-06-08', NULL, 78, 13, NULL),
(10, '2011-09-12', NULL, 60, 24, NULL);

COMMIT;

-- -------------------------------------
-- Data for table bar
-- -------------------------------------
START TRANSACTION;

INSERT INTO bar
(bar_id, per_id, bar_name, bar_notes)
VALUES
(NULL, 6, 'Florida bar', NULL),
(NULL, 7, 'Alabama bar', NULL),
(NULL, 8, 'Georgia bar', NULL),
(NULL, 9, 'Michigan bar', NULL),
(NULL, 10, 'South Carolina bar', NULL),
(NULL, 6, 'Montana bar', NULL),
(NULL, 7, 'Arizona Bar', NULL),
(NULL, 8, 'Nevada Bar', NULL),
(NULL, 9, 'New York Bar', NULL),
(NULL, 10, 'New York Bar', NULL),
(NULL, 6, 'Mississippi Bar', NULL),
(NULL, 7, 'California Bar', NULL),
(NULL, 8, 'Illinois Bar', NULL),
(NULL, 9, 'Indiana Bar', NULL),
(NULL, 10, 'Illinois Bar', NULL),
(NULL, 6, 'Tallahassee Bar', NULL),
(NULL, 7, 'Ocala Bar', NULL),
(NULL, 8, 'Bay County Bar', NULL),
(NULL, 9, 'Cincinatti Bar', NULL);

COMMIT;

-- -------------------------------------
-- Data for table speciality
-- -------------------------------------
START TRANSACTION;

INSERT INTO speciality
(spc_id, per_id, spc_type, spc_notes)
VALUES
(NULL, 6, 'business', NULL),
(NULL, 7, 'traffic', NULL),
(NULL, 8, 'bankruptcy', NULL),
(NULL, 9, 'insurance', NULL),
(NULL, 10, 'judicial', NULL),
(NULL, 6, 'environmental', NULL),
(NULL, 7, 'cirminal', NULL),
(NULL, 8, 'real estate', NULL),
(NULL, 9, 'malpractice', NULL);

COMMIT;

-- -------------------------------------
-- Data for table court
-- -------------------------------------
START TRANSACTION;

INSERT INTO court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
VALUES
(NULL, 'leon county circuit court', '301 south monroe street', 'tallahassee', 'fl', 323035292, 8506065504, 'lccc@us.fl.gov', 'http://www.leoncountycircuitcourt.gov/', NULL),
(NULL, 'leon county traffic court', '1921 thomasville road', 'tallahassee', 'fl', 323035292, 8505774100, 'lctc@us.fl.gov', 'http://www.leoncountytrafficcourt.gov/', NULL),
(NULL, 'florida supreme court', '500 south duval street', 'tallahassee', 'fl', 323035292, 8504880125, 'fsc@us.fl.gov', 'http://www.floridasupremecourt.org/', NULL),
(NULL, 'orange county courthouse', '424 north orange avenue', 'orlando', 'fl', 328012248, 4078362000, 'occ@us.fl.gov', 'http://www.ninthcircuit.org/', NULL),
(NULL, 'fifth district court of appeal', '300 south beach street', 'daytona beach', 'fl', 321158763, 3862258600, '5dca@us.fl.gov', 'http://www.5dca.org/', NULL);

COMMIT;

-- -------------------------------------
-- Data for table judge
-- -------------------------------------
START TRANSACTION;

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
(11, 5, 150000, 10, NULL),
(12, 4, 185000, 3, NULL),
(13, 4, 135000, 2, NULL),
(14, 3, 170000, 6, NULL),
(15, 1, 120000, 1, NULL);

COMMIT;

-- -------------------------------------
-- Data for table judge_hist
-- -------------------------------------
START TRANSACTION;

INSERT INTO judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(NULL, 11, 3, '2009-01-16', 'i', 130000, NULL),
(NULL, 12, 2, '2010-05-27', 'i', 140000, NULL),
(NULL, 13, 5, '2000-01-02', 'i', 115000, NULL),
(NULL, 13, 4, '2005-07-05', 'i', 135000, NULL),
(NULL, 14, 4, '2008-12-09', 'i', 155000, NULL),
(NULL, 15, 1, '2011-03-17', 'i', 120000, 'freshman justice'),
(NULL, 11, 5, '2010-07-05', 'i', 150000, 'assigned to another court'),
(NULL, 12, 4, '2012-10-08', 'i', 165000, 'became chief justice'),
(NULL, 14, 3, '2009-04-19', 'i', 170000, 'reassigned to court based upon local area population growth');

COMMIT;

-- -------------------------------------
-- Data for table `case`
-- -------------------------------------
START TRANSACTION;

INSERT INTO `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL, 13, 'civil', 'client says that his logo is being used without his consent to promote a rival business', '2010-09-09', NULL, 'copyright infringeemnt'),
(NULL, 12, 'criminal', 'client is charged with assaulting her husband during an argument', '2009-11-18', '2010-12-23', 'assault'),
(NULL, 14, 'civil', 'client broke an ankle while shopping at a local grocery store. no wet floor sign was posted although the floor had just been mopped.', '2008-05-06', '2008-07-23', 'slip and fall'),
(NULL, 11, 'criminal', 'client was charged with stealing several televisions from his former place of employment. client has a solid alibi', '2011-05-20', NULL, 'grand theft'),
(NULL, 13, 'criminal', 'client charged with possession of 10 grams of cocaine, allegedly found in his glove box by state police', '2011-06-05', NULL, 'possession of narcotics'),
(NULL, 14, 'civil', 'client alleges newspaper printed false information about his personal activities while he ran a large laundry business in a small nearby town.', '2007-01-19', '2007-05-20', 'defamation'),
(NULL, 12, 'criminal', 'client charged with the murder of his co-worker over a lovers feud. client has no alibi', '2010-03-20', NULL, 'murder'),
(NULL, 15, 'civil', 'client made the horrible mistake of selecting a degree other than IT and had to declare bankruptcy.', '2012-01-26', '2013-02-28', 'bankruptcy');

COMMIT;

-- -------------------------------------
-- Data for table assignment
-- -------------------------------------
START TRANSACTION;

INSERT INTO assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES
(NULL, 1, 6, 7, NULL),
(NULL, 2, 6, 6, NULL),
(NULL, 3, 7, 2, NULL),
(NULL, 4, 8, 2, NULL),
(NULL, 5, 9, 5, NULL),
(NULL, 1, 10, 1, NULL),
(NULL, 2, 6, 3, NULL),
(NULL, 3, 7, 8, NULL),
(NULL, 4, 8, 8, NULL),
(NULL, 5, 9, 8, NULL),
(NULL, 4, 10, 4, NULL);

COMMIT;

/*
-- Securing data...
-- Demo sha2() hash function:
select sha2('test',512);

-- length as hex value:
select length(sha2('test',512));

-- sha2() hash function converted to binary
select unhex(sha2('test',512));

-- length as biary
select length(unhex(sha2('test',512)));

Example: hash string "test"
-- SHA2/512 as hexadecimal digits
SELECT SHA2("test", 512) AS 'SHA2/512 Hashed Hexadecimal Digits',
    LENGTH(UNHEX(SHA2("test", 512))) AS 'hexadecimal Byte Count';

-- SHA@/512 as binary bytes (Note: My SQL console cannot display binary data.)
SELECT UNHEX(SHA2("test", 512)) AS 'Binary String',
    LENGTH(UNHEX(SHA2("test", 512))) AS 'Binary Byte Count';
*/

-- populate person SSN number
-- Students do not need to create a stored procedure to populate SSNs

-- person
DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN
    DECLARE x, y INT;
    SET x = 1;
    -- dynamically set loop ending value (total number of persons)
    select count(*) into y from person;
    -- select y; -- display number of persons (only for testing)

    WHILE x <= y DO

    -- RAND([N]): Returns random floating-point value v in the range 0 <= v < 1.0
    -- Documentation: https://dev.mysql.com/doc/refman/5.7/en/mathematical-functions.html#function_rand
    -- randomize ssn between 000000001 and 999999999 (note: using value 000000000 for ex. 4 below)
    update person
    set per_ssn=(SELECT unhex(sha2(FLOOR(000000001 + (RAND() * 1000000000)), 512)))
    where per_id=x;

    SET x = x + 1;

    END WHILE;

END$$
DELIMITER ;
call CreatePersonSSN();

show warnings;

-- show populated per_ssn fields (must use hex() function to convert binary data into readable format)
select 'show populated per_ssn fields after calling stored proc' as '';
select per_id, length(per_ssn) from person order by per_id;
DO SLEEP(7);

DROP PROCEDURE IF EXISTS CreatePersonSSN;

-- ################## DISPLAY TABLE DATA ##################

-- verify table data: pause 5 seconds for each query result set
select 'person table (auto_increment): per_id 1-15' as '';
select * from person;
DO SLEEP(3);
-- or...
-- select sleep(5); -- though, returns 0

select 'phone table: per_id not all persons have phones (e.g., per_id 3)' as '';
select * from phone;
do sleep(5);

select 'client table: per_id 1-5' as '';
select * from client;
do sleep(5);

select 'attorney table: per_id 6-10' as '';
select * from attorney;
do sleep(5);

select 'speciality table: attorneys per_id 6-10' as '';
select * from speciality;
do sleep(5);

select 'bar table: attorneys per_id 6-10' as '';
select * from bar;
do sleep(5);

select 'court table: (auto_increment)' as '';
select * from court;
do sleep(5);

select 'judge table: per_id 11-15, crt_id 1-5' as '';
select * from judge;
do sleep(5);

select 'judge_hist table: per_id 11-15, jhs_crt 1-5' as '';
select * from judge_hist;
do sleep(5);

select 'case table, judges per_id 11-15' as '';
select * from `case`;
do sleep(5);

select 'assignment table: per_cid 1-5, per_aid 6-10, cse_id 1-8' as '';
select * from assignment;
do sleep(5);

-- Demo: create error in assignment table above (semicolon instead of comma)
select'Can use exit for debugging--like this...:' as '';
EXIT

-- ################## BEGIN REPORTS ##################

-- 1) Create a view that displays attorneys’ *full* names, *full* addresses, ages, hourly rates, the bar names that they’ve passed, as well as their specialties, sort by attorneys’
-- last names.

/*
If the view does not exist, CREATE OR REPLACE VIEW is the same as CREATE VIEW.
If the view does exist, CREATE OR REPLACE VIEW is the same as ALTER VIEW.
Example:
CREATE or REPLACE VIEW v_attorney_info AS
*/

drop VIEW if exists v_attorney_info;
CREATE VIEW v_attorney_info AS

    select
    concat(per_lname, ", ", per_fname) as name,
    concat(per_street, ", ", per_city, ", ", per_state, " ", per_zip) as address,
    TIMESTAMPDIFF(year, per_dob, now()) as age,
    CONCAT('$', FORMAT(aty_hourly_rate, 2)) as hourly_rate,
    bar_name, spc_type
    from person
        natural join attorney
        natural join bar
        natural join speciality
        order by per_lname;

-- or, for MS SQL Server
/*
select
concat(per_lname, ", ", per_fname) as name
concat(per_street, ", ", per_city, ", ", per_state, " ", per_zip) as address,
TIMESTAMPDIFF(year, per_dob, now()) as new_age,
CONCAT('$R', FORMAT(aty_hourly_rate, 2)) as hourly rate,
bar_name, spc_type
from person p
    join attorney a on p.per_id=a.per_id
    join bar b on a.per_id=b.per_id
    join speciality s on a.per_id=s.per_id
    order by per_lname;
*/

select 'display view v_attorney_info' as '';

select * from v_attorney_info;
drop VIEW if exists v_attorney_info;
do sleep(5);


-- 2) Create a stored procedure that displays how many judges were born in each month of the year, sorted by month.
-- Step through solution...

select 'Step a) Display all persons DOB months: testing MySQL monthname() function' as '';

select per_id, per_fname, per_lname, per_dob, monthname(per_dob) from person;
do sleep(5);

select 'Step b) Display pertinent judge data' as '';

select p.per_id, per_fname, per_lname, per_dob, per_type from person as p natural join judge as j;
do sleep(5);

select 'Final: Step c) Stored proc: Display month numbers, month names, and how many judges were born each month' as '';

drop procedure if exists sp_num_judges_born_by_month;
DELIMITER //
CREATE PROCEDURE sp_num_judges_born_by_month()
BEGIN
    select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
    from person
    natural join judge
    group by month_name
    order by month;
END //
DELIMITER ;

select 'calling sp_num_judges_born_by_month()' as '';

CALL sp_num_judges_born_by_month();
do sleep(5);

drop procedure if exists sp_num_judges_born_by_month;

-- NOTE: prior to MySQL 5.5 use...
-- DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(per_dob, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(per_dob, '00-%m-%d')) AS age,
-- do sleep(5);

-- 3) Create a stored procedure that displays *all* case types and descriptions, as well as judges’ *full* names, *full* addresses, phone numbers, years in practice, for cases that
-- they presided over, with their start and end dates, sort by judges’ last names.
/* demo what happens with no back ticks around case, quotation marks DO NOT work! */

drop procedure if exists sp_cases_and_judges;
DELIMITER //
CREATE PROCEDURE sp_cases_and_judges()
BEGIN

-- check query result sets of associated tables:
/*
select per_id from person;
select  * from judge;
select * from phone;
*/

select per_id, cse_id, cse_type, cse_description,
    concat(per_fname, " ", per_lname) as name,
    concat('(',substring(phn_num, 1, 3), ')', substring(phn_num, 4, 3), '-', substring(phn_num, 7, 4)) as judge_office_num,
    phn_type,
    jud_years_in_practice,
    cse_start_date,
    cse_end_date
from person
    natural join judge
    natural join `case`
    natural join phone
where per_type='j'
order by per_lname;

END //
DELIMITER ;

select 'calling sp_cases_and_judges()' as '';

CALL sp_cases_and_judges();
do sleep(5);
drop procedure if exists sp_cases_and_judges;

-- 4) Create a trigger that automatically adds a record to the judge history table for every record added to the judge table.

select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.' as '';
do sleep(5);

-- Note: technically, judge could already be in the person table, yet not added to the judge table. Or, here, add a new person.
-- For person: OK to use NULL for per_ssn. Though, a better solution...binary encrypted SSN

select 'show person data *before* adding person record' as '';
select per_id, per_fname, per_lname from person;
do sleep(5);

-- give person a unique randomized salt, thne hash and salt SSN
SET @safe=RANDOM_BYTES(64); -- salt includes unique random bytes for each user
SET @num=000000000; -- Note: already provided random SSN from 111111111 - 999999999, inclusive above
SET @ssn=unhex(sha2(concat( @salt, @num), 512));    -- salt and hash person's SSN 000000000

INSERT INTO person
(per_id, per_ssn, per_safe, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, @ssn, @salt, 'Bobby', 'Sue', '123 Main St', 'Panama City Beach', 'FL', 3245330221, 'bsue@fl.gov', '1962-05-16', 'j', 'new district judge');

select 'show person data *after* adding person record' as '';
select per_id, per_fname, per_lname from person;
do sleep (5);

-- 5) Create a trigger that automatically adds a record to the judge history table for every record modified in the judge table.

select 'show judge/judge_hist data *before* AFTER UPDATE trigger fires (trg_judge_history_after_update)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

-- after update of judge table, insert into judge_hist table
DROP TRIGGER IF EXISTS trg_judge_history_after_update;
DELIMITER  //
CREATE TRIGGER trg_judge_history_after_update
AFTER UPDATE ON judge
FOR EACH ROW
BEGIN
    -- Note: concatenated data that goes into jhs_notes: same as AFTER INSERT trigger
    INSERT INTO judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
    VALUES
    (
        NEW.per_id, NEW.crt_id, current_timestamp(), 'u', NEW.jud_salary,
        concat("modifying user: ", user(), " Notes: ", NEW.jud_notes)
    );
END //
DELIMITER ;

select 'fire trigger by updating latest judge entry (salary and notes)' as '';
do sleep(5);

UPDATE judge
SET jud_salary=190000, jud_notes='senior justice - longest serving member'
WHERE per_id=16;

select 'show judge/judge_hist data *after* AFTER UPDATE trigger fires (trg_judge_history_after_update)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

DROP TRIGGER IF EXISTS trg_judge_history_after_update;

-- 6) Create a one-time event that executes one hour following its creation, the event should add a judge record (one more than the required five records), have the event call a stored
-- procedure that adds the record (name it one_time_add_judge).

-- Here: use current_user(), rather than user(), because it was the creator of the event who caused the trigger to fire
-- See jhs_notes after event fires

-- a) create stored procedure that will be used in event
-- NOTE: can create a new person, or use an existing person record, as per below.
drop procedure if exists sp_add_judge_record;
DELIMITER //

CREATE PROCEDURE sp_add_judge_record()
BEGIN
    INSERT INTO judge
    (per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
    VALUES
    (6, 1, 110000, 0, concat("New judge was former attorney. ", "Modifying event creator: ", current_user()));
    END //

    DELIMITER ;

select '1) check event_scheduler' as '';
SHOW VARIABLES LIKE 'event_scheduler';
do sleep(5);

select '2) if not, turn it on...' as '';
SET GLOBAL event_scheduler = ON;

select '3) recheck event_scheduler' as '';
SHOW VARIABLES LIKE 'event_scheduler';
do sleep(5);

select 'Demo: use stored proc to add judge record after 5 seconds. Note: insert will also fire trigger for judge history' as '';
do sleep(5);

select 'show judge/judge_hist data *before* event fires (one_time_add_judge)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

DROP EVENT IF EXISTS one_time_add_judge;
-- temporarily redefine delimiter
DELIMITER //
CREATE EVENT IF NOT EXISTS one_time_add_judge
ON SCHEDULE
    AT NOW() + INTERVAL 5 SECOND
COMMENT ' adds a judge record only one-time'
DO
BEGIN
    CALL sp_add_judge_record();
END//

DELIMITER ;

select 'show events from rnr06;' as '';
-- list events for database
SHOW EVENTS FROM rnr06;
do sleep(5);

select 'show state of event scheduler: show processlist;' as '';
show processlist;
do sleep(5);

select 'show judge/judge_hist data *after* event fires (one_time_add_judge)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

-- ################## EXTRA CREDIT ##################
/*
Create a scheduled event that will run every two months, beginning in three weeks, and runs for the next four years, starting from the creation date. The event should not allow
more than the first 100 judge histories to be stored, thereby removing all others (name it remove_judge_history).
*/

-- Extra Credit solution:
/*
DROP EVENT IF EXISTS remove_judge_history;
-- temporarily redefine delimiter
DELIMITER //
CREATE EVENT IF NOT EXISTS remove_judge_history
ON SCHEDULE
    EVERY 2 MONTH
STARTS NOW() + INTERVAL 3 WEEK
ENDS NOW() + INTERVAL 4 YEAR
COMMENT 'keeps only the first 100 judge records'
DO
BEGIN
    DELETE FROM judge_hist where jhs_id > 100;
END ??

-- change delimiter back
DELIMITER ;

-- DROP EVENT IF EXISTS remove_judge_history;
*/

-- USE THIS ONE AS DEMO
-- Demo: deletes 1 judge history every 2 seconds, beginning 5 seconds after creation, running for 30 seconds
DROP EVENT IF EXISTS remove_judge_history;
-- temporarily redefine delimiter
DELIMITER //
CREATE EVENT IF NOT EXISTS remove_judge_history
ON SCHEDULE
    EVERY 2 SECOND
STARTS NOW() + INTERVAL 5 SECOND
ENDS NOW() + INTERVAL 30 SECOND
COMMENT 'deletes all judge histories'
DO
BEGIN
    DELETE FROM judge_hist ORDER BY jhs_id desc LIMIT 1;
END//

-- change delimiter back
DELIMITER ;

select 'show events from rnr06;' as '';
-- list events for database
SHOW EVENTS FROM rnr06;
do sleep(5);

select 'show state of event scheduler: show processlist;' as '';
show processlist;
do sleep(5);
