SET ANSI_WARNINGS ON;
GO

use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name=N'rnr06')
DROP DATABASE rnr06;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'rnr06')
CREATE DATABASE rnr06;
GO

use rnr06;
GO

------------------------------------
-- Table person
------------------------------------
IF OBJECT_ID (N'dbo.person', N'U') IS NOT NULL
DROP TABLE dbo.person;
GO

CREATE TABLE dbo.person
(
    per_id SMALLINT not null identity(1,1),
    per_ssn binary(64) NULL,
    per_salt binary(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_gender CHAR(1) NOT NULL CHECK (per_gender IN('m','f')),
    per_dob DATE NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL DEFAULT 'FL',
    per_zip int NOT NULL check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email VARCHAR(100) NULL,
    per_type CHAR(1) NOT NULL CHECK (per_type IN('c', 's')),
    per_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT ux_per_ssn unique nonclustered (per_ssn ASC)
);
GO

------------------------------------
-- Table phone
------------------------------------
IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
GO

CREATE TABLE dbo.phone
(
    phn_id SMALLINT NOT NULL identity(1,1),
    per_id SMALLINT NOT NULL,
    phn_num bigint NOT NULL check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) NOT NULL CHECK (phn_type IN('h','c','w','f')),
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    CONSTRAINT fk_phone_person
        FOREIGN KEY (per_id)
        REFERENCES dbo.person (per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

------------------------------------
-- Table slsrep
------------------------------------
IF OBJECT_ID (N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep;
GO

CREATE TABLE dbo.slsrep
(
	per_id SMALLINT not null,
    srp_yr_sales_goal decimal(8,2) NOT NULL check (srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) NOT NULL check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(7,2) NOT NULL check (srp_ytd_comm >= 0),
    srp_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_slsrep_person
        FOREIGN KEY (per_id)
        REFERENCES dbo.person (per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

------------------------------------
-- Table customer
------------------------------------
IF OBJECT_ID (N'dbo.customer', N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

CREATE TABLE dbo.customer
(
    per_id SMALLINT not null,
    cus_balance decimal(7,2) NOT NULL check (cus_balance >= 0),
    cus_total_sales decimal(7,2) NOT NULL check (cus_total_sales >= 0),
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_customer_person
        FOREIGN KEY (per_id)
        REFERENCES dbo.person (per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table srp_hist
------------------------------------
IF OBJECT_ID (N'dbo.srp_hist', N'U') IS NOT NULL
DROP TABLE dbo.srp_hist;
GO

CREATE TABLE dbo.srp_hist
(
    sht_id SMALLINT not null identity(1,1),
    per_id SMALLINT not null,
    sht_type char(1) not null CHECK (sht_type IN('i','u','d')),
    sht_modified datetime not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) NOT NULL check (sht_yr_sales_goal >= 0),
    sht_yr_total_sales decimal(8,2) NOT NULL check (sht_yr_total_sales >= 0),
    sht_yr_total_comm decimal(7,2) NOT NULL check (sht_yr_total_comm >= 0),
    sht_notes VARCHAR(255) NULL,
    PRIMARY KEY (sht_id),

    CONSTRAINT fk_srp_hist_slsrep
        FOREIGN KEY (per_id)
        REFERENCES dbo.slsrep (per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table contact
------------------------------------
IF OBJECT_ID (N'dbo.contact', N'U') IS NOT NULL
DROP TABLE dbo.contact;
GO

CREATE TABLE dbo.contact
(
    cnt_id int NOT NULL identity(1,1),
    per_cid smallint NOT NULL,
    per_sid smallint NOT NULL,
    cnt_date datetime NOT NULL,
    cnt_notes varchar(255) NULL,
    PRIMARY KEY (cnt_id),

    CONSTRAINT fk_contact_customer
        FOREIGN KEY (per_cid)
        REFERENCES dbo.customer (per_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    CONSTRAINT fk_contact_slsrep
        FOREIGN KEY (per_sid)
        REFERENCES dbo.slsrep (per_id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);
GO

------------------------------------
-- Table [order]
------------------------------------
IF OBJECT_ID (N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order];
GO

CREATE TABLE dbo.[order]
(
    ord_id int NOT NULL identity(1,1),
    cnt_id int NOT NULL,
    ord_placed_date DATETIME NOT NULL,
    ord_filled_date DATETIME NULL,
    ord_notes VARCHAR(255) NULL,
    PRIMARY KEY (ord_id),

    CONSTRAINT fk_order_contact
        FOREIGN KEY (cnt_id)
        REFERENCES dbo.contact (cnt_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table region
------------------------------------
IF OBJECT_ID (N'dbo.region', N'U') IS NOT NULL
DROP TABLE dbo.region;
GO

CREATE TABLE dbo.region
(
    reg_id TINYINT NOT NULL identity(1,1),
    reg_name CHAR(1) NOT NULL,
    reg_notes VARCHAR(255) NULL,
    PRIMARY KEY (reg_id)
);
GO

------------------------------------
-- Table state
------------------------------------
IF OBJECT_ID (N'dbo.state', N'U') IS NOT NULL
DROP TABLE dbo.state;
GO

CREATE TABLE dbo.state
(
    ste_id TINYINT NOT NULL identity(1,1),
    reg_id TINYINT NOT NULL,
    ste_name CHAR(2) NOT NULL DEFAULT 'FL',
    ste_notes VARCHAR(255) NULL,
    PRIMARY KEY (ste_id),
    
    CONSTRAINT fk_state_region
        FOREIGN KEY (reg_id)
        REFERENCES dbo.region (reg_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table city
------------------------------------
IF OBJECT_ID (N'dbo.city', N'U') IS NOT NULL
DROP TABLE dbo.city;
GO

CREATE TABLE dbo.city
(
    cty_id SMALLINT NOT NULL identity(1,1),
    ste_id TINYINT NOT NULL,
    cty_name VARCHAR(30) NOT NULL,
    cty_notes VARCHAR(255) NULL,
    PRIMARY KEY (cty_id),

    CONSTRAINT fk_city_state
        FOREIGN KEY (ste_id)
        REFERENCES dbo.state (ste_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table store
------------------------------------
IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store
(
    str_id SMALLINT NOT NULL identity(1,1),
    cty_id SMALLINT NOT NULL,
    str_name VARCHAR(45) NOT NULL,
    str_street VARCHAR(30) NOT NULL,
    str_city VARCHAR(30) NOT NULL,
    str_state CHAR(2) NOT NULL DEFAULT 'FL',
    str_zip int NOT NULL check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone bigint NOT NULL check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email VARCHAR(100) NOT NULL,
    str_url VARCHAR(100) NOT NULL,
    str_notes VARCHAR(255) NULL,
    PRIMARY KEY (str_id),

    CONSTRAINT fk_store_city
        FOREIGN KEY (cty_id)
        REFERENCES dbo.city (cty_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table invoice
------------------------------------
IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice;
GO

CREATE TABLE dbo.invoice
(
    inv_id int NOT NULL identity(1,1),
    ord_id int NOT NULL,
    str_id SMALLINT NOT NULL,
    inv_date DATETIME NOT NULL,
    inv_total DECIMAL(8,2) NOT NULL check (inv_total >= 0),
    inv_paid bit NOT NULL,
    inv_notes VARCHAR(255) NULL,
    PRIMARY KEY (inv_id),

    CONSTRAINT ux_ord_id unique nonclustered (ord_id ASC),

    CONSTRAINT fk_invoice_order
        FOREIGN KEY (ord_id)
        REFERENCES dbo.[order] (ord_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    CONSTRAINT fk_invoice_store
        FOREIGN KEY (str_id)
        REFERENCES dbo.store (str_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table payment
------------------------------------
IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment;
GO

CREATE TABLE dbo.payment
(
    pay_id int NOT NULL identity(1,1),
    inv_id int NOT NULL,
    pay_date DATETIME NOT NULL,
    pay_amt DECIMAL(7,2) NOT NULL check (pay_amt >= 0),
    pay_notes VARCHAR(255) NULL,
    PRIMARY KEY (pay_id),

    CONSTRAINT fk_payment_invoice
        FOREIGN KEY (inv_id)
        REFERENCES dbo.invoice (inv_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table vendor
------------------------------------
IF OBJECT_ID (N'dbo.vendor', N'U') IS NOT NULL
DROP TABLE dbo.vendor;
GO

CREATE TABLE dbo.vendor
(
    ven_id SMALLINT NOT NULL identity(1,1),
    ven_name VARCHAR(45) NOT NULL,
    ven_street VARCHAR(30) NOT NULL,
    ven_city VARCHAR(30) NOT NULL,
    ven_state CHAR(2) NOT NULL DEFAULT 'FL',
    ven_zip int NOT NULL check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone bigint NOT NULL check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email VARCHAR(100) NOT NULL,
    ven_url VARCHAR(100) NOT NULL,
    ven_notes VARCHAR(255) NULL,
    PRIMARY KEY (ven_id)
);
GO

------------------------------------
-- Table product
------------------------------------
IF OBJECT_ID (N'dbo.product', N'U') IS NOT NULL
DROP TABLE dbo.product;
GO

CREATE TABLE dbo.product
(
    pro_id SMALLINT NOT NULL identity(1,1),
    ven_id SMALLINT NOT NULL,
    pro_name VARCHAR(30) NOT NULL,
    pro_descript VARCHAR(45) NULL,
    pro_weight FLOAT NOT NULL check (pro_weight >= 0),
    pro_qoh SMALLINT NOT NULL check (pro_qoh >= 0),
    pro_cost DECIMAL(7,2) NOT NULL check (pro_cost >= 0),
    pro_price DECIMAL(7,2) NOT NULL check  (pro_price >= 0),
    pro_discount DECIMAL(3,0) NULL,
    pro_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id),

    CONSTRAINT fk_product_vendor
        FOREIGN KEY (ven_id)
        REFERENCES dbo.vendor (ven_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table product_hist
------------------------------------
IF OBJECT_ID (N'dbo.product_hist', N'U') IS NOT NULL
DROP TABLE dbo.product_hist;
GO

CREATE TABLE dbo.product_hist
(
    pht_id int NOT NULL identity(1,1),
    pro_id SMALLINT NOT NULL,
    pht_date DATETIME NOT NULL,
    pht_cost DECIMAL(7,2) NOT NULL check (pht_cost >= 0),
    pht_price DECIMAL(7,2) NOT NULL check (pht_price >= 0),
    pht_discount DECIMAL(3,0) NULL,
    pht_notes VARCHAR(255) NULL,
    PRIMARY KEY (pht_id),

    CONSTRAINT fk_product_hist_product
        FOREIGN KEY (pro_id)
        REFERENCES dbo.product (pro_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table order_line
------------------------------------
IF OBJECT_ID (N'dbo.order_line', N'U') IS NOT NULL
DROP TABLE dbo.order_line;
GO

CREATE TABLE dbo.order_line
(
    oln_id int NOT NULL identity(1,1),
    ord_id int NOT NULL,
    pro_id SMALLINT NOT NULL,
    oln_qty SMALLINT NOT NULL check (oln_qty >= 0),
    oln_price DECIMAL(7,2) NOT NULL check (oln_price >= 0),
    oln_notes VARCHAR(255) NULL,
    PRIMARY KEY (oln_id),

    CONSTRAINT fk_order_line_order
        FOREIGN KEY (ord_id)
        REFERENCES dbo.[order] (ord_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    CONSTRAINT fk_order_line_product
        FOREIGN KEY (pro_id)
        REFERENCES dbo.product (pro_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO

------------------------------------
-- Table time
------------------------------------
IF OBJECT_ID (N'dbo.time', N'U') IS NOT NULL
DROP TABLE dbo.time;
GO

CREATE TABLE dbo.time
(
    tim_id INT NOT NULL identity(1,1),
    tim_yr SMALLINT NOT NULL,
    tim_qtr TINYINT NOT NULL,
    tim_month TINYINT NOT NULL,
    tim_week TINYINT NOT NULL,
    tim_day TINYINT NOT NULL,
    tim_time TIME NOT NULL,
    tim_notes VARCHAR(255) NULL,
    PRIMARY KEY (tim_id)
);
GO

------------------------------------
-- Table sale
------------------------------------
IF OBJECT_ID (N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale;
GO

CREATE TABLE dbo.sale
(
    pro_id SMALLINT NOT NULL,
    str_id SMALLINT NOT NULL,
    cnt_id INT NOT NULL,
    tim_id INT NOT NULL,
    sal_qty SMALLINT NOT NULL,
    sal_price DECIMAL(8,2) NOT NULL,
    sal_total DECIMAL(8,2) NOT NULL,
    sal_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id, cnt_id, tim_id, str_id),

    CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
    unique NONCLUSTERED (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),

    CONSTRAINT fk_sale_time
        FOREIGN KEY (tim_id)
        REFERENCES dbo.time (tim_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    CONSTRAINT fk_sale_contact
        FOREIGN KEY (cnt_id)
        REFERENCES dbo.contact (cnt_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    
    CONSTRAINT fk_sale_store
        FOREIGN KEY (str_id)
        REFERENCES dbo.store (str_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    CONSTRAINT fk_sale_product
        FOREIGN KEY (pro_id)
        REFERENCES dbo.product (pro_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
GO


SELECT * FROM information_schema.tables;

------------------------------------
-- Data for table person
------------------------------------
INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1, NULL, 'Steve', 'Rogers', 'm', '1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', 's', NULL),
(2, NULL, 'Bruce', 'Wayne', 'm', '1968-03-20', '1007 Mountain Drive', 'Gotham', 'NY', 983208440, 'bwayne@knology.net', 's', NULL),
(3, NULL, 'Peter', 'Parker', 'm', '1988-09-12', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', 's', NULL),
(4, NULL, 'Jane', 'Thompson', 'f', '1978-05-08', '13563 Ocean View Drive', 'Seattle', 'WA', 132084409, 'jthompson@gmail.com', 's', NULL),
(5, NULL, 'Debra', 'Steele', 'f', '1994-07-19', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', 's', NULL),
(6, NULL, 'Tony', 'Smith', 'm', '1972-05-04', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', 'c', NULL),
(7, NULL, 'Hank', 'Pymi', 'm', '1980-08-28', '2355 Brown Street', 'Cleveland', 'OH', 822348890, 'hpym@aol.com', 'c', NULL),
(8, NULL, 'Bob', 'Best', 'm', '1992-02-10', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', 'c', NULL),
(9, NULL, 'Sandra', 'Smith', 'f', '1990-01-26', '87912 Lawrence Ave', 'Atlanta', 'GA', 682348890, 'sdole@gmail.com', 'c', NULL),
(10, NULL, 'Ben', 'Avery', 'm', '1983-12-24', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', 'c', NULL),
(11, NULL, 'Arthur', 'Curry', 'm', '1975-12-15', '3304 Euclid Avenue', 'Miami', 'FL', 342219932, 'acurry@gmail.com', 'c', NULL),
(12, NULL, 'Diana', 'Price', 'f', '1980-08-22', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', 'c', NULL),
(13, NULL, 'Adam', 'Smith', 'm', '1995-01-31', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', 'c', NULL),
(14, NULL, 'Judy', 'Sleen', 'f', '1970-03-22', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@symaptico.com', 'c', NULL),
(15, NULL, 'Bill', 'Neiderham', 'm', '1982-06-13', '43567 Netherland Blve', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', 'c', NULL),
(16, NULL, 'Susan', 'Morgan', 'f', '1984-05-15', '2001 Water Street', 'Dublin', 'CA', 905260018, 'smorgan@gmail.com', 's', NULL),
(17, NULL, 'Josh', 'Cunningham', 'm', '1994-03-28', '2974 Heavener Avenue', 'Conyers', 'GA', 302076580, 'jcunningham@comcast.net', 's', NULL),
(18, NULL, 'Bradley', 'Delphino', 'm', '1992-12-05', '2882 Frum Street', 'Nashville', 'TN', 378209480, 'brad_delphino@yahoo.com', 'c', NULL),
(19, NULL, 'Joy', 'Hammond', 'f', '1986-11-02', '1513 Kelley Road', 'Biloxi', 'MS', 839531480, 'joy_hammond@gmail.com', 'c', NULL),
(20, NULL, 'Marshall', 'Tucker', 'm', '1978-06-12', '1869 Todds Lane', 'San Antonio', 'TX', 782259904, 'mtucker@hotmail.com', 's', NULL);
GO

select * from dbo.person;

CREATE PROC dbo.CreatePersonSSN
AS
BEGIN
    DECLARE @salt BINARY(64);
    DECLARE @ran_num int;
    DECLARE @ssn binary(64);
    DECLARE @x INT, @y INT;
    SET @x = 1;

    SET @y=(select count(*) from dbo.person);
    
    WHILE (@x <= @y)
    BEGIN

    SET @salt=CRYPT_GEN_RANDOM(64);
    set @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
    SET @ssn=HASHBYTES('SHA2_512', concat(@salt, @ran_num));

    update dbo.person
    set per_ssn=@ssn, per_salt=@salt
    where per_id=@x;

    SET @x = @x + 1;

    END;

END;
GO

exec dbo.CreatePersonSSN

------------------------------------
-- Data for table phone
------------------------------------
INSERT INTO dbo.phone
(per_id, phn_num, phn_type, phn_notes)
VALUES
(2, '5462457260', 'h', NULL),
(3, '5214367502', 'c', NULL),
(4, '5103847526', 'w', NULL),
(1, '3257409504', 'f', NULL),
(4, '7462418501', 'c', NULL),
(6, '5845023458', 'c', NULL),
(8, '2148650214', 'w', NULL),
(2, '7591549824', 'h', NULL),
(4, '6522145762', 'f', NULL),
(5, '2016480265', 'c', NULL);

select * from dbo.phone;

------------------------------------
-- Data for table slsrep
------------------------------------
INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, NULL),
(2, 80000, 35000, 3500, NULL),
(3, 150000, 84000, 9650, 'Great salesperson!'),
(4, 125000, 87000, 15300, NULL),
(5, 98000, 43000, 8750, NULL),
(6, 120000, 85500, 10500, NULL),
(7, 150250, 65500, 24500, NULL),
(8, 110500, 75000, 68500, NULL),
(9, 99000, 45000, 25500, NULL),
(10, 125000, 90500, 62500, NULL);

select * from dbo.slsrep;

------------------------------------
-- Data for table customer
------------------------------------
INSERT INTO dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES
(6, 120, 14789, NULL),
(7, 98.46, 234.92, NULL),
(8, 0, 4578, 'Customer always pays on time.'),
(9, 981.73, 1672.38, 'High balance.'),
(10, 541.23, 782.57, NULL),
(11, 251.02, 13782.96, 'Good customer.'),
(12, 582.67, 963.12, 'Previously paid in full.'),
(13, 121.67, 1057.45, 'Recent customer.'),
(14, 765.43, 6789.42, 'Buys bulk quantities.'),
(15, 304.39, 456.81, 'Has not purchased recently.'),
(16, 154.89, 458.69, NULL),
(17,254.35, 548.15, NULL),
(18, 849.65, 458.15, NULL),
(19, 451.26, 524.13, NULL),
(20, 345.25, 458.95, NULL);

select * from dbo.customer;

------------------------------------
-- Data for table contact
------------------------------------
INSERT INTO dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES
(1, 6, '1999-01-01', NULL),
(2, 6, '2001-09-29', NULL),
(3, 7, '2002-08-15', NULL),
(2, 7, '2002-09-01', NULL),
(4, 7, '2004-01-05', NULL),
(5, 8, '2004-02-28', NULL),
(4, 8, '2004-03-03', NULL),
(1, 9, '2004-04-07', NULL),
(5, 9, '2004-07-29', NULL),
(3, 11, '2005-05-02', NULL),
(4, 13, '2005-06-14', NULL),
(2, 15, '2005-07-02', NULL),
(4, 12, '2001-03-09', NULL),
(3, 8, '2002-08-15', NULL),
(2, 6, '2003-04-25', NULL),
(1, 8, '2004-12-04', NULL),
(5, 11, '2001-11-19', NULL);

select * from dbo.contact;

------------------------------------
-- Data for table [order]
------------------------------------
INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1, '2010-11-23', '2010-12-24', NULL),
(2, '2005-03-19', '2005-07-28', NULL),
(3, '2011-07-01', '2011-07-06', NULL),
(4, '2009-12-12', '2010-01-05', NULL),
(5, '2008-09-21', '2008-11-26', NULL),
(6, '2009-04-17', '2009-04-30', NULL),
(7, '2010-05-31', '2010-06-07', NULL),
(8, '2007-09-02', '2007-09-16', NULL),
(9, '2011-12-08', '2011-12-13', NULL),
(10, '2012-02-29', '2012-05-02', NULL),
(11, '2008-02-14', '2018-03-26', NULL),
(12, '2010-03-18', '2010-06-23', NULL),
(13, '2008-12-23', '2019-05-19', NULL),
(14, '2009-10-26', '2010-11-23', NULL),
(15, '2008-09-12', '2009-10-23', NULL);

select * from dbo.[order];

------------------------------------
-- Data for table region
------------------------------------
INSERT INTO dbo.region
(reg_name, reg_notes)
VALUES
('c', NULL),
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL),
('c', NULL),
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL);
GO

select * from dbo.region;

------------------------------------
-- Data for table state
------------------------------------
INSERT INTO dbo.state
(reg_id, ste_name, ste_notes)
VALUES
(1, 'MI', NULL),
(3, 'IL', NULL),
(4, 'WA', NULL),
(5, 'FL', NULL),
(2, 'TX', NULL),
(4, 'OR', NULL),
(5, 'GA', NULL),
(1, 'OH', NULL),
(2, 'FL', NULL),
(3, 'MS', NULL);
GO

select * from dbo.state;

------------------------------------
-- Data for table city
------------------------------------
INSERT INTO dbo.city
(ste_id, cty_name, cty_notes)
VALUES
(1, 'Detroit', NULL),
(2, 'Aspen', NULL),
(2, 'Chicago', NULL),
(3, 'Clover', NULL),
(4, 'St. Louis', NULL),
(5, 'San Antonio', NULL),
(6, 'Naples', NULL),
(6, 'Cleveland', NULL),
(7, 'Starkville', NULL),
(8, 'Atlanta', NULL);
GO

select * from dbo.city;

------------------------------------
-- Data for table store
------------------------------------
INSERT INTO dbo.store
(cty_id, str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
(2, 'Walgreens', '14567 Walnut Ln', 'Aspen', 'IL', '475315690', '3127658127', 'info@walgreens.com', 'http://www.walgreens.com', NULL),
(3, 'CVS', '572 Csper Rd', 'Chicago', 'IL', '505231519', '3128926534', 'help@cvs.com', 'http://www.cvs.com', 'Rumor of merger.'),
(4, 'Lowes', '81309 Catapult Ave', 'Clover', 'WA', '802345671', '9017653421', 'sales@lowes.com', 'http://www.lowes.com', NULL),
(5, 'Walmart', '14567 Walnut Ln', 'St. Louis', 'FL', '387563628', '8722718923', 'info@walmart.com', 'http://www.walmart.com', NULL),
(1, 'Dollar General', '47583 Davison Rd', 'Detroit', 'MI', '482983456', '3137583492', 'ask@dollargeneral.com', 'http://www.dollargeneral.com', 'recently sold property'),
(6, 'Home Depot', '3199 Crestview Terrace', 'San Antonio', 'TX', '782195482', '8306603916', 'info@homedepot.com', 'http://www.homedepot.com', NULL),
(7, 'Target', '530 Bingamon Road', 'Cleveland', 'OH', '441145840', '4405756836', 'sales@target.com', 'http://www.target.com', NULL),
(8, 'Piggly Wiggly', '1599 Desperado Street', 'Atlanta', 'GA', '303035405', '4047362142', 'help@pigglywiggly.com', 'http://www.pigglywiggly.com', NULL),
(9, 'Publix', '1707 Brownton Road', 'Starkville', 'MS', '397594068', '6623247031', 'info@publix.com', 'http://www.publix.com', 'new location'),
(10, 'Winn-Dixie', '3836 Owen Lane', 'Naples', 'FL', '339405846', '2392130452', 'help@winndixie.com', 'http://www.winndixie.com', NULL);
GO

select * from dbo.store;

------------------------------------
-- Data for table invoice
------------------------------------
INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES
(5, 1, '2001-05-03', 58.32, 0, NULL),
(4, 1, '2006-11-11', 100.59, 0, NULL),
(1, 1, '2010-09-16', 57.34, 0, NULL),
(3, 2, '2011-01-10', 99.32, 1, NULL),
(2, 3, '2008-06-24', 1109.67, 1, NULL),
(6, 4, '2009-04-20', 239.83, 0, NULL),
(7, 5, '2010-06-05', 537.29, 0, NULL),
(8, 2, '2007-09-09', 644.21, 1, NULL),
(9, 3, '2011-12-17', 934.12, 1, NULL),
(10, 4, '2012-03-18', 27.45, 0, NULL),
(11, 2, '2008-12-08', 654.25, 0, NULL),
(12, 4, '2010-03-25', 418.12, 1, NULL),
(13, 5, '2011-03-27', 65.12, 0, NULL),
(14, 2, '2009-11-14', 125.23, 0, NULL),
(15, 1, '2012-08-25', 98.25, 1, NULL);

select * from dbo.invoice;

------------------------------------
-- Data for table vendor
------------------------------------
INSERT INTO dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES
('Sysco', '531 Dolphin Run', 'Orlando', 'FL', '344761234', '7641238543', 'sales@sysco.com', 'http://www.sysco.com', NULL),
('General Electric', '100 Happy Trails Dr.', 'Boston', 'MA', '123458743', '2134569641', 'support@ge.com', 'http://www.ge.com', 'Very good turnaround'),
('Cisco', '300 Cisco Dr.', 'Stanford', 'OR', '872315492', '7823456723', 'cisco@cisco.com', 'http://www.cisco.com', NULL),
('Goodyear', '100 Goodyear Dr.', 'Gary', 'IN', '485321956', '5784218427', 'sales@goodyear.com', 'http://www.goodyear.com', 'Competing well with Firestone.'),
('Snap-On', '42185 Magenta Ave', 'Lake Falls', 'ND', '387513649', '9197345632', 'support@snapon.com', 'http://www.snap-on.com', 'Good quality tools!'),
('Samsung', '3360 Horizon Circle', 'Tacoma', 'WA', '984025846', '2536825265', 'help@samsung', 'http://www.samsung.com', NULL),
('Wrangler', '1080 Monroe Street', 'Houston', 'TX', '770235408', '7134732239', 'info@wrangler.com', 'http://www.wrangler.com', 'Quality tires'),
('Microsoft', '1106 Saints Alley', 'Tampa', 'FL', '336021547', '8137626031', 'sales@microsoft.com', 'http://www.microsoft.com', NULL),
('BF Goodrich', '441 Old House Drive', 'Pompano Beach', 'FL', '330664805', '7409468145', 'support@bfgoodrich.com', 'http://www.bfgoodrich.com', NULL),
('Party Time, Inc.', '2607 Richison Drive', 'Stoneham', 'CO', '807542985', '4063239575', 'info@partytime.com', 'http://www.partytimeinc.com', NULL);

select * from dbo.vendor;

------------------------------------
-- Data for table product
------------------------------------
INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(1, 'hammer', '', 2.5, 45, 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, 'screwdriver', '', 1.8, 120, 1.99, 3.49, NULL, NULL),
(4, 'pail', '16 Gallon', 2.8, 48, 3.89, 7.99, 40, NULL),
(5, 'cooking oil', 'Peanut oil', 15, 19, 19.99, 28.99, NULL, 'gallons'),
(3, 'frying pan', '', 3.5, 178, 8.45, 13.99, 50, 'Currently 1/2 price sale.'),
(6, 'tire', 'mud terrain', 8.5, 110, 79.99, 149.99, NULL, NULL),
(7, 'software', 'editing program', 1.2, 45, 40.99, 75.99, NULL, NULL),
(8, 'streamers', '', 1.1, 65, 1.99, 5.99, NULL, NULL),
(9, 'helium balloons', 'multi-colored', 1.1, 125, 0.99, 1.99, NULL, NULL),
(10, 'smartphone', '', 2.5, 25, 450.99, 675.99, NULL, NULL);

select * from dbo.product;

------------------------------------
-- Data for table order_line
------------------------------------
INSERT INTO dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
VALUES
(1, 2, 10, 8.0, NULL),
(2, 3, 7, 9.88, NULL),
(3, 4, 3, 6.99, NULL),
(5, 1, 2, 12.76, NULL),
(4, 5, 13, 58.99, NULL),
(6, 4, 12, 45.99, NULL),
(7, 2, 10, 8.59, NULL),
(8, 3, 8, 12.99, NULL),
(9, 4, 1, 21.99, NULL),
(10, 2, 8, 18.49, NULL),
(11, 1, 2, 45.99, NULL),
(12, 4, 11, 75.99, NULL),
(13, 2, 14, 12.99, NULL),
(14, 5, 3, 25.99, NULL),
(15, 3, 4, 18.99, NULL);

select * from dbo.order_line;

------------------------------------
-- Data for table payment
------------------------------------
INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(5, '2008-07-01', 5.99, NULL),
(4, '2010-09-28', 4.99, NULL),
(1, '2008-07-23', 8.75, NULL),
(3, '2010-10-31', 19.55, NULL),
(2, '2011-03-29', 32.50, NULL),
(6, '2010-10-03', 20.00, NULL),
(8, '2008-08-09', 1000.00, NULL),
(9, '2009-01-10', 103.68, NULL),
(7, '2007-03-15', 25.00, NULL),
(10, '2007-05-12', 40.00, NULL),
(4, '2007-05-22', 9.33, NULL),
(7, '2004-05-12', 15.98, NULL),
(8, '2010-02-28', 24.99, NULL),
(4, '2011-12-14', 45.98, NULL),
(6, '2012-04-15', 75.98, NULL),
(10, '2010-11-26', 120.99, NULL);

select * from dbo.payment;

------------------------------------
-- Data for table product_hist
------------------------------------
INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, '2005-02-03 09:13:56', 1.99, 3.49, NULL, NULL),
(3, '2005-03-04 23:21:49', 3.89, 7.99, 40, NULL),
(4, '2006-05-06 18:09:04', 19.99, 28.99, NULL, 'gallons'),
(5, '2006-05-07 15:07:29', 8.45, 13.99, 50, 'Currently 1/2 price sale.'),
(6, '2004-02-15 14:28:45', 12.99, 24.99, 25, NULL),
(7, '2006-04-26 12:05:26', 5.65, 9.25, 10, NULL),
(8, '2005-12-04 16:49:02', 7.99, 15.99, 15, NULL),
(9, '2004-02-25 23:45:26', 11.89, 18.99, 20, NULL),
(10, '2006-11-20 08:15:49', 3.99, 8.99, 25, NULL);

select * from dbo.product_hist;

------------------------------------
-- Data for table time
------------------------------------
INSERT INTO dbo.time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
VALUES
(2008, 2, 5, 19, 7, '11:59:59', NULL),
(2010, 4, 12, 49, 4, '08:34:21', NULL),
(1999, 4, 12, 52, 5, '05:21:34', NULL),
(2011, 3, 8, 36, 1, '09:32:18', NULL),
(2001, 3, 7, 27, 2, '23:56:32', NULL),
(2008, 1, 1, 5, 4, '04:22:36', NULL),
(2010, 2, 4, 14, 5, '02:49:11', NULL),
(2014, 1, 2, 8, 2, '12:27:14', NULL),
(2013, 3, 9, 38, 4, '10:12:28', NULL),
(2012, 4, 11, 47, 3, '22:36:22', NULL),
(2014, 2, 6, 23, 3, '19:07:10', NULL),
(2011, 4, 11, 25, 4, '15:02:15', NULL),
(2015, 2, 5, 16, 2, '08:15:26', NULL),
(2001, 3, 7, 30, 3, '14:25:03', NULL),
(2004, 1, 9, 24, 5, '23:01:15', NULL),
(2006, 5, 3, 29, 3, '13:25:26', NULL);
GO

select * from dbo.time;

------------------------------------
-- Data for table sale
------------------------------------
INSERT INTO dbo.sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
VALUES
(1, 5, 5, 3, 20, 9.99, 199.8, NULL),
(2, 4, 6, 2, 5, 5.99, 29.95, NULL),
(3, 3, 4, 1, 30, 3.99, 119.7, NULL),
(4, 2, 1, 5, 15, 18.99, 284.85, NULL),
(5, 1, 2, 4, 6, 11.99, 71.94, NULL),
(5, 2, 5, 6, 10, 9.99, 199.8, NULL),
(4, 3, 6, 7, 5, 5.99, 29.95, NULL),
(3, 1, 4, 8, 30, 3.99, 119.7, NULL),
(2, 3, 1, 9, 15, 18.99, 284.85, NULL),
(1, 4, 2, 10, 6, 11.99, 71.94, NULL),
(1, 2, 3, 11, 10, 11.99, 119.9, NULL),
(2, 5, 3, 10, 12, 7.99, 29.98, NULL),
(3, 2, 2, 8, 15, 15.99, 35.99, NULL),
(4, 3, 4, 7, 20, 25.99, 45.98, NULL),
(5, 3, 6, 9, 10, 11.99, 32.98, NULL),
(2, 1, 3, 2, 5, 4.99, 15.98, NULL),
(2, 4, 5, 5, 30, 14.99, 28.85, NULL),
(4, 2, 1, 4, 25, 5.99, 15.99, NULL),
(1, 2, 3, 9, 25, 2.99, 9.99, NULL),
(5, 5, 2, 2, 10, 17.99, 31.98, NULL),
(5, 3, 4, 7, 15, 12.99, 26.99, NULL),
(4, 4, 4, 9, 5, 16.99, 32.99, NULL),
(1, 1, 6, 10, 15, 21.99, 38.98, NULL),
(5, 2, 1, 2, 25, 11.99, 24.99, NULL),
(2, 5, 2, 10, 30, 24.99, 40.99, NULL);

GO

select * from dbo.sale;

------------------------------------
-- Data for table srp_hist
------------------------------------
INSERT INTO srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
VALUES
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL),
(4, 'i', getDate(), SYSTEM_USER, getDate(), 150000, 175000, 17500, NULL),
(3, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL),
(2, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL),
(5, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL),
(6, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 120000, 15000, NULL),
(7, 'i', getDate(), SYSTEM_USER, getDate(), 250000, 275000, 15500, NULL),
(8, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 155000, 14500, NULL),
(9, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 220000, 250000, 25000, NULL),
(10, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 245000, 210000, 2800, NULL);

select * from dbo.srp_hist;

select year(sht_date) from dbo.srp_hist;


-- %%%%%%%%%%%%%%%%%%% BEGIN REPORTS %%%%%%%%%%%%%%%%%%%

select  * from [rnr06].information_schema.tables;
go

select  * from [rnr06].information_schema.columns;
go

sp_help 'dbo.srp_hist';
GO

-- 1) Create a stored procedure (product_days_of_week) listing the product names, descriptions, and the day of the week in which they were sold, in ascending order of the day of week.
use rnr06;
GO

print '#1 Solution: Create a stored procedure (product_days_of_week) listing the product names, descriptions, ' + CHAR(13)+CHAR(19) + 'and the day of the week in which they were sold, in ascending order of the day of week:

';

IF OBJECT_ID (N'dbo.product_days_of_week', N'P') IS NOT NULL
DROP PROC dbo.product_days_of_week;
GO

CREATE PROC dbo.product_days_of_week AS
BEGIN
-- DATENAME ( datepart, date )
-- tim_day is tinyint, not date. Compensate with offset. Sunday default start of week.
select pro_name, pro_descript, datename(dw, tim_day-1) 'day_of_week'
from product p
    join sale s on p.pro_id=s.pro_id
    join time t on t.tim_id=s.tim_id
order by tim_day-1 asc;     --sorts numerically, rather than string return of datename() function
END
GO

--call stored procedure
exec dbo.product_days_of_week;

--list all rpocedures (e.g., stored procedures or functions) for database
select * from rnr06.information_schema.routines
GO

sp_helptext 'dbo.product_days_of_week'
GO



-- 2) Create a stored procedure (product_drill_down) listing the product name, quantity on hand, store name, city name, state name, and region name where each product was purchased, in descending order of
-- quantity on hand.
print '#2 Solution: Create a stored procedure (product_drill_down) listing the product name, quantity on hand, store name, ' + CHAR(13)+CHAR(10) + 'city name, state name, and region name where each product was 
purchased, in descending order of quantity on hand.

';

--1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.product_drill_down', N'P') IS NOT NULL
DROP PROC dbo.product_drill_down;
GO

CREATE PROC dbo.product_drill_down AS
BEGIN
select pro_name, pro_qoh,
    FORMAT(pro_cost, 'C', 'en-us') as cost,
    FORMAT(pro_price, 'C', 'en-us') as price,
    str_name, cty_name, ste_name, reg_name
    from product p
        join sale s on p.pro_id=s.pro_id
        join store sr on sr.str_id=s.str_id
        join city c on sr.cty_id=c.cty_id
        join state st on c.ste_id=st.ste_id
        join region r on st.reg_id=r.reg_id
    order by pro_qoh desc;
END
GO

--call stored procedure
exec dbo.product_drill_down;

--list all rpocedures (e.g., stored procedures or functions) for database
select * from rnr06.information_schema.routines
where routine_type = 'PROCEDURE';
GO



-- 3) Create a stored procedure (add_payment) that adds a payment record. Use variables and pass suitable arguments.
print '#3 Solution: Create a stored procedure (add_payment) that adds a payment record. Use variables and pass suitable arguments.

';

--1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.add_payment', N'P') IS NOT NULL
DROP PROC dbo.add_payment;
GO

CREATE PROC dbo.add_payment
    @inv_id_p int,
    @pay_date_p datetime,
    @pay_amt_p decimal(7,2),
    @pay_notes_p varchar(255)
AS
BEGIN
--don't need pay_id pk because it is auto-increment
insert into payment(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(@inv_id_p, @pay_date_p, @pay_amt_p, @pay_notes_p);
END
GO
print 'list table data before call:

';

select * from payment;

DECLARE
    @inv_id_v int = 6,
    @pay_date_v DATETIME = '2014-01-05 11:56:38',
    @pay_amt_v DECIMAL(7,2) = 159.99,
    @pay_notes_v VARCHAR(255) = 'testing add_payment';

--call stored procedure
exec dbo.add_payment @inv_id_v, @pay_date_v, @pay_amt_v, @pay_notes_v;

print 'list table data after call:

';
select * from payment;

--list all rpocedures (e.g., stored procedures or functions) for database
select * from rnr06.information_schema.routines
where routine_type = 'PROCEDURE';
GO

sp_helptext 'dbo.add_payment'
GO

drop PROC dbo.add_payment;



-- 4) Create a stored procedure (customer_balance) listing the customer’s id, name, invoice id, total paid on invoice, balance (derived attribute from the difference of a customer’s invoice total and their\
-- respective payments), pass customer’s last name as argument—which may return more than one value.
print '#4 Solution: Create a stored procedure (customer_balance) listing the customer’s id, name, invoice id, total paid on invoice, ' + CHAR(13)+CHAR(10) + 'balance (derived attribute from the difference of a
customer’s invoice total and their respective payments), ' + CHAR(13)+CHAR(10) + 'pass customer’s last name as argument—which may return more than one value.

';

--1st arg is an object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.customer_balance', N'TR') IS NOT NULL
DROP PROC dbo.customer_balance
GO

CREATE PROC dbo.customer_balance
@per_lname_p varchar(30)
AS
BEGIN
    select p.per_id, per_fname, per_lname, i.inv_id, inv_total,
    FORMAT(sum(pay_amt), 'C', 'en-us') as total_paid,
    FORMAT((inv_total - sum(pay_amt)), 'C', 'en-us') as invoice_diff
    from person p
        join dbo.customer c on p.per_id=c.per_id
        join dbo.contact ct on c.per_id=ct.per_cid
        join dbo.[order] o on ct.cnt_id=o.cnt_id
        join dbo.invoice i on o.ord_id=i.ord_id
        join dbo.payment pt on i.inv_id=pt.inv_id
        --must be contained in group by, if not used in aggregate function
    where per_lname=@per_lname_p
    group by p.per_id, i.inv_id, per_lname, per_fname, inv_total;
END
GO

DECLARE @per_lname_v varchar(30) = 'smith';

exec dbo.customer_balance @per_lname_v;

select * from test.information_schema.routines
where routine_type = 'PROCEDURE';
GO

sp_helptext 'dbo.customer_balance'
GO

drop PROC dbo.customer_balance;

select * from time order by tim_yr;

select * from sale order by tim_id;

select st.str_id, sal_total, tim_yr
from store st
    join sale s on st.str_id=s.str_id
    join time t on s.tim_id=t.tim_id
where tim_yr between 2010 and 2013;


-- 5) Create and display the results of a stored procedure (store_sales_between_dates) that lists each store's id, sum of total sales (formatted), and years for a given time period, by passing the start/end
-- dates, group by years, and sort by total sales then years, both in descending order.
print '#5 Solution: Create and display the results of a stored procedure (store_sales_between_dates) that lists each store''s id, ' + CHAR(13)+CHAR(10) + 'sum of total sales (formatted), and years for a given
time period, by passing the start/end dates, group by years, ' +CHAR(13)+CHAR(10) + 'and sort by total sales then years, both in descending order.

';

IF OBJECT_ID(N'dbo.store_sales_between_dates', N'TR') IS NOT NULL
DROP PROC dbo.store_sales_between_dates
GO

CREATE PROC dbo.store_sales_between_dates
@start_date_p smallint,
@end_date_p smallint
AS
BEGIN
    select st.str_id, FORMAT(sum(sal_total), 'C', 'en-us') as 'total sales', tim_yr as year
    from store st
        join sale s on st.str_id=s.str_id
        join time t on s.tim_id=t.tim_id
    where tim_yr between @start_date_p and @end_date_p
    group by tim_yr, st.str_id
    order by sum(sal_total) desc, tim_yr desc;
END
GO

DECLARE
@start_date_v smallint = 2010,
@end_date_v smallint = 2013;

exec dbo.store_sales_between_dates @start_date_v, @end_date_v;

select * from test.information_schema.routines
where routine_type = 'PROCEDURE';
GO

sp_helptext 'dbo.store_sales_between_dates'
GO

drop PROC dbo.store_sales_between_dates;



--Steps for Ex. 6 below...
select * from invoice;
select * from payment order by inv_id;

select i.inv_id, sum(pay_amt) total_paid
from invoice i
    join payment pon i.inv_id=p.inv_id
group by i.inv_id
order by i.inv_id;

select i.inv_id,
    sum(pay_amt) as total_paid,
    (inv_total - sum(pay_amt)) invoice_diff
from invoice i
    join payment p on i.inv_id=p.inv_id
group by i.inv_id, inv_total
order by invoice_diff;



-- 6) Create a trigger (trg_check_inv_paid) that updates an invoice record, after a payment has been made, indicating whether or not the invoice has been paid.
print'#6 Solution: Create a trigger (trg_check_inv_paid) that updates an invoice record, after a payment has been made, ' + CHAR(13)+CHAR(10) + 'indicating whether or not the invoice has been paid.

';

IF OBJECT_ID(N'dbo.trg_check_inv_paid', N'TR') IS NOT NULL
DROP TRIGGER dbo.trg_check_inv_paid
GO

CREATE TRIGGER dbo.trg_check_inv_paid
ON dbo.payment
AFTER INSERT AS
BEGIN

update invoice
set inv_paid=0;

UPDATE invoice
SET inv_paid=1
FROM invoice as i
    JOIN
    (
        SELECT inv_id, sum(pay_amt) as total_paid
        FROM payment
        GROUP BY inv_id
    ) as v ON i.inv_id=v.inv_id
    WHERE total_paid >= inv_total;

END
GO

print 'list table data before trigger fires (resets all invoices to unpaid, 0):

';
select * from invoice;
select * from payment;

INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(3, '2014-07-04', 75.00, 'Paid by check.');

print 'list table data after trigger fires (after trigger validation, displays updates inv_paid attribute):

';
select * from invoice;
select * from payment;

select inv_id, sum(pay_amt) as sum_pmt
from payment
group by inv_id;

SELECT * FROM sys.triggers;
GO

DROP TRIGGER dbo.trg_check_inv_paid;
GO



-- %%%%%%%%%%%%%%%%%%% EXTRA CREDIT %%%%%%%%%%%%%%%%%%%
--Create and display the results of a stored procedure (order_line_total) that calculates the total price for each order line, based upon the product price times quantity, which yields a subtotal (oln_price),
--total column includes 6% sales tax. Query result set should display order line id, product id, name, description, price, order line quantity, subtotal (oln_price), and total with 6% sales tax. Sort by product ID.
print '***Extra Credit*** Solution: Create and display the results of a stored procedure (order_line_total) that calculates the total price for each order line, ' + CHAR(13)+CHAR(10) + 'based upon the product 
price times quantity, which yields a subtotal (oln_price), total column includes 6% sales tax. ' + CHAR(13)+CHAR(10) + 'Query result set should display order line id, product id, name, description, price, order line quantity, 
subtotal (oln_price), and total with 6% sales tax. Sort by product ID.

';

IF OBJECT_ID(N'dbo.order_line_total', N'P') IS NOT null
DROP PROC dbo.order_line_total
GO

CREATE PROC dbo.order_line_total AS
BEGIN
    select oln_id, p.pro_id, pro_name, pro_descript,
    FORMAT(pro_price, 'C', 'en-us') as pro_price,
    oln_qty,
    FORMAT((oln_qty * pro_price), 'C', 'en-us') as oln_price,
    FORMAT((oln_qty * pro_price) * 1.06, 'C', 'en-us') as total_with_6pct_tax
    from product p
    join order_line ol on p.pro_id=ol.pro_id
    order by p.pro_id;
END
GO

exec dbo.order_line_total;

select * from test.information_schema.routines
where routine_type = 'PROCEDURE';
GO
