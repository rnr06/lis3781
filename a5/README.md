> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Rhianna Reichert

### Assignment 5 Requirements:

*Two Parts:*

1. Create database in MS SQL Server
2. Chapter Questions
    - Chapter 15

#### README.md file should include the following items:

* Screenshot of MS SQL code
* Screenshot of populated ERD
* Bitbucket repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of A5 ERD*

|        A5 ERD        |                                   
|:---------------------------------:|
|              ![A5 ERD](img/a5_erd.png "A5 ERD")              |


*Screenshot of A5 SQL Solutions*

|        A5 Solution #1        |       A5 Solution #2   |       A5 Solution #3   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![A5 Solution #1](img/solution_1.png "A5 Solution #1")              |                    ![A5 Solution #2](img/solution_2.png "A5 Solution #2")|                    ![A5 Solution #3](img/solution_3.png "A5 Solution #3")


|        A5 Solution #4        |      A5 Solution #5   |       A5 Solution #6   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![A5 Solution #4](img/solution_4.png "A5 Solution #4")              |                    ![A5 Solution #5](img/solution_5.png "A5 Solution #5")|                    ![A5 Solution #6](img/solution_6.png "A5 Solution #6")


*Screenshot of A5 SQL Results*

|        A5 Results #1        |       A5 Results #2   |       A5 Results #3   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![A5 Results #1](img/results_1.png "A5 Results #1")              |                    ![A5 Results #2](img/results_2.png "A5 Results #2")|                    ![A5 Results #3](img/results_3.png "A5 Results #3")


|        A5 Results #4        |      A5 Results #5   |       A5 Results #6   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![A5 Results #4](img/results_4.png "A5 Results #4")              |                    ![A5 Results #5](img/results_5.png "A5 Results #5")|                    ![A5 Results #6](img/results_6.png "A5 Results #6")


*Screenshot of A5 Extra Credit*

|        A5 Extra Credit Solution        |       A5 Extra Credit Results   |                                    
|:---------------------------------:|:--------------------------------------------:|
|              ![A5 Extra Credit Solution #1](img/extra_credit_solution.png "A5 Extra Credit Solution #1")              |                    ![A5 Extra Credit Results](img/extra_credit_results.png "A5 Extra Credit Results")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
