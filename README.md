> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Rhianna Reichert

### Assignment Requirements:

*Assignment Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Intall AMPPS (*ONLY* if not previously installed)
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Screenshot of SQL code
    - Screenshot of populated tables
        * 1.) company table
        * 2.) customer table
    - Bitbucket repo link

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshots of Oracle SQL code
    - Screenshots of populated tables
        * 1.) customer table
        * 2.) commodity table
        * 3.) order table
    - Bitbucket repo link

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Screenshot of MS SQL code
    - Screenshot of populated ERD
    - Bitbucket repo link

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Screenshot of MS SQL code
    - Screenshot of populated ERD
    - Bitbucket repo link

### Project Requirements:

*Project Links:*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Screenshot of MySQL code
    - Screenshot of populated ERD
    - Bitbucket repo link

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Screenshot(s) of Mongo DB Shell Command
    - Screenshot of Optional JSON code for required reports
    - Bitbucket repo link


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")