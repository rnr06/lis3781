-- hash and salt data:
-- Hashing sensitive data (i.e., SSN): one-way obfuscation (can't be unhashed -- only compared)
-- Note: encryption can be unencrypted using a key.
-- https://security.stackexchange.com/questions/17421/how-to-store-salt

-- Using salted hash values prevent precomputation attacks, such as "rainbow" tables.
-- Rainbow table: precomputed table ofr reversing cryptographic hash functions--usually for cracking passwork hashes.
-- Resulting message digest is product of both sensitive data (i.e., password/ssn) and salt value--will not match anything in rainbow table~
-- What is "pepper"?
-- Pepper: specified value added to input such as a password prior to being hashed.
-- Pepper performs similar role to salt, but while salt is stored alongside hashed output, pepper is not.
-- Should you "pepper" as well? Some say "No.":
-- https://stackoverflow.com/questions/16891729/best-practices-salting-peppering-passwords

-- generic hashing:
select md5('abc');
select md5('123');
-- precomputed rainbow tables will show a list of common passwords
900150983cd24fb0d6963f7d28e17f72 abc
202cb962ac59075b964b07152d234b70 123
etc.

-- There are SHA2 rainbow tables as well. So, while cryptographically more complex, this doesn't help!
SELECT SHA2('abc', 512);

-- pseudocode solution:
-- stored_value=hash(concat(sensitive_data + salt))
-- Testing:
select RANDOM_BYTES(64);    -- prepare salt
select length(RANDOM_BYTES(64));    -- return required length

-- Practice:
set @salt=RANDOM_BYTES(64);
set @sensitive_data='abc';
SELECT SHA2(concat(@salt, @sensitive_data), 512);   -- salt/hash sensitive data
SELECT length(SHA2(concat(@salt, @sensitive_data), 512));   -- length of salt/hash sensitive data

-- create binary version to save space, and to add further obfuscation
SELECT unhex(SHA2(concat(@salt, @sensitive_data), 512));
SELECT length(unhex(SHA2(concat(@salt, @sensitive_data), 512)));    -- half the storage size!

-- implementation
-- NOTE: *MUST* use SSL connection when implementing!
-- Otherwise, sensitive data may be seen over the network--*before* being obfuscated!
-- Also, randomized salts (should be stored) with hashed sensitive data for future comparisons.
-- Rather than providing same salt for all secured data--if salt is compromised, so is *all* of the data!
drop database if exists hash;
create database if not exists hash;
use hash;

drop table if exists user;
create table user
(
    uid int unsigned primary key auto_incement,
    ssn binary(64) not null,
    salt binary(64) not null
);

set @ssn='abc'; // Another reason to obfuscate data--users *will* choose similar passwords! :(
set @salt=RANDOM_BYTES(64);
-- Note: Because uid is auto increment, do not need to include here. Only included for explanatory completeness.
-- In fact, while this works in MySQL, MS SQL Server would not allow it! Simply don't include the auto increment pk.
INSERT INTO user
(uid,ssn,salt)
VALUES
(NULL, unhex(SHA2(CONCAT(@salt, @ssn),512)), @salt);    -- stores binary version of salted and hashed SSN

select * from user; -- Note: command line cannot read binary data, and will display gibberish

-- can't "unhash" value--only can compare
select uid from user
where ssn=UNHEX(sha2(concat(salt, 'abc'), 512));    -- matches! :)