> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Rhianna Reichert

### Assignment 2 Requirements:

*Four Parts:*

1. Locally create rnr06 database using SQL
2. Locally create two tables using SQL
    * a.) company table
    * b.) customer table
3. Create two different users with different passwords
4. Chapter Questions
    - Chapter 11

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables
    * 1.) company table
    * 2.) customer table
* Bitbucket repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of A2 SQL Code*

|        A2 SQL Code - 1        |       A2 SQL Code - 2   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![A2 SQL Code - 1](img/lis3781_a2_solutions_1.png "A2 SQL Code - 1")              |                    ![A2 SQL Code - 2](img/lis3781_a2_solutions_2.png "A2 SQL Code - 2")                  |

|        A2 SQL Code - 3        |                                     
|:---------------------------------:|
|              ![A2 SQL Code - 3](img/lis3781_a2_solutions_3.png "A2 SQL Code - 3")                 |


*Screenshot of A2 Populated Tables*

|        A2 Company Table        |       A2 Customer Table   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![A2 Company Table](img/company_table.png "A2 Company Table")              |                    ![A2 Customer Table](img/customer_table.png "A2 Customer Table")




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
