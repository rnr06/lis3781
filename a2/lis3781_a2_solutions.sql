/*
A character set is a set of symbols and encodings.
A collation is a set of rules for comparing characters in a character set.

Suppose that we havr an alphabet with four letters: "A", "B", "a", "b".
We give each leter a number: "A" = 0, "B" = 1, "a" = 2, "b" = 3.
The letter "A" is a symbol, the number 0 is the encoding for "A",
and the combination of all four letters and their encodings is a character set.

Suppose that we want to compare two string values, "A" and "B".
The simplest way to do this is to look at the encodings: 0 for "A" and 1 for "B".
Because 0 is less than 1, we say "A" is less than "B". What we've just done is apply a collation to our character set.
The collation is a set of rules (only one rule in this case): "compare the encodings."
We call this simplest of all possible collations a binary collation.

http://dev.mysql.com/doc/refman/5.5/en/charset.html
*/

-- set foreign_key_checks=0;

drop database if exists rnr06;
create database if not exists rnr06;
use rnr06;

-- ---------------------------------------
-- Table company
-- ---------------------------------------
DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company
(
    cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
    cmp_street VARCHAR(30) NOT NULL,
    cmp_city VARCHAR(30) NOT NULL,
    cmp_state CHAR(2) NOT NULL,
    cmp_zip INT(9) unsigned ZEROFILL NOT NULL COMMENT 'no dashes',
    cmp_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
    cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL COMMENT '12,345,678.90',
    cmp_email VARCHAR(100) NULL,
    cmp_url VARCHAR(100) NULL,
    cmp_notes VARCHAR(255) NULL,
    PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES
(null,'C-Corp','8218 Low Street','Wilmington','NC','284042584','9195823405','52184601.00',null,'http://www.technology.com','company notes1'),
(null,'S-Corp','8315 Swanson Road','Greensboro','NC','274985102','9195420365','2450384.00',null,'http://www.codingfordummies.com','company notes2'),
(null,'Non-Profit-Corp','7299 North Art Street','Fayetteville','NC','283054026','9192453218','15703684.00',null,'http://www.rhiannanreichert.com','company notes3'),
(null,'LLC','1335 Rinehart  Road','Asheville','NC','288014036','9195623084','85472684.00',null,'http://www.reichert.com','company notes4'),
(null,'Partnership','2810 Watson Lane','Maggie Valley','NC','287515843','9195420354','5148739.00',null,'http://www.fsu.edu.com','company notes5');

SHOW WARNINGS;

-- ---------------------------------------
-- Table customer
-- ---------------------------------------
DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
    cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_id INT UNSIGNED NOT NULL,
    cus_ssn binary(64) NOT NULL,
    cus_ini binary(64) NOT NULL,
    cus_type ENUM('Loyal','Discount','Impulse','Need-Based','Wandering'),
    cus_first VARCHAR(15) NOT NULL,
    cus_last VARCHAR(30) NOT NULL,
    cus_street VARCHAR(30) NULL,
    cus_city VARCHAR(30) NULL,
    cus_state CHAR(2) NULL,
    cus_zip int(9) UNSIGNED ZEROFILL NULL,
    cus_phone bigint UNSIGNED NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
    cus_email VARCHAR(100) NULL,
    cus_balance DECIMAL(7,2) UNSIGNED NULL COMMENT '12,345.67',
    cus_tot_sales DECIMAL(7,2) UNSIGNED NULL,
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (cus_id),

    UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
    INDEX idx_cmp_id (cmp_id ASC),

    /*
    Comment CONSTRAINT line to demo DBMS auto value when *not* using "constraint" option for foreign keys, then...
    SHOW CREATE TABLE customer;
    */
    CONSTRAINT fk_customer_company
        FOREIGN KEY (cmp_id)
        REFERENCES company (cmp_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

-- salting and hashing sensitive data (e.g., SSN). Normally, *each* record would receive unique random salt!
set @salt=RANDOM_BYTES(64);

INSERT INTO customer
VALUES
(null,2,unhex(SHA2(CONCAT(@salt, 000456789),512)),@salt,'Discount','Wilson','Carraway','4167 Jennifer Lane','Raleigh','NC','276045810','9194582485','wcarraway@mymail.com','6581.35','95840.35','customer notes1'),
(null,4,unhex(SHA2(CONCAT(@salt, 001456789),512)),@salt,'Loyal','Betty','Dennis','3979 Red Dog Road','Matthews','NC','281054802','9195035481','betty_dennis@mymail.com','1505.32','5403.32','customer notes2'),
(null,3,unhex(SHA2(CONCAT(@salt, 002456789),512)),@salt,'Impulse','Linda','Klein','3905 Johnson Street','Fayetteville','NC','283010542','9195410358','lklein@mymail.com','851.03','4502.68','customer notes3'),
(null,5,unhex(SHA2(CONCAT(@salt, 003456789),512)),@salt,'Need-Based','Davis','Williard','4448 Keyser Ridge Road','High Point','NC','272602145','9195420358','davywilliard@mymail.com','5184.68','12584.21','customer notes4'),
(null,1,unhex(SHA2(CONCAT(@salt, 004456789),512)),@salt,'Wandering','Sandra','Aarons','2106 Patton Lane','Durham','NC','277046024','9195480354','sandra_aarons@mymail.com','621.84','2403.95','customer notes5');

SHOW WARNINGS;
-- set  foreign_key_checks=1;

select * from company;
select * from customer;

-- be sure to create the following db and tables above...
-- then, using a command prompt or source file, use the following line to show how to generate  homework files...

-- local tee Windows (place notee at end of output):
tee E:/Courses/_LIS3781/lis3781_a2_reports.sql

-- or... also, Windows
-- tee D:\Courses\_LIS3781\lis3781_a2_reports.sql

-- Mac users:
-- tee /usr/local/mysql/data/lis3781_a2_reports.sql

-- remote (CCI Server) tee:
-- tee /home/rnr06/db/lis3781_a2_reports.sql

-- NOTE: tee filestream closed when user logs out, must reopen with each user

-- 1. Limit user1 to select, update, and delete provileges on campany and customer tables

/*
Twp-step process:
Create new user with CREATE USER statement, then use GRANT statement:
I) CREATE USER 'usernmae'@'localhost' IDENTIFIED BY 'password':