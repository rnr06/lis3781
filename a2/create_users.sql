-- First: create users:
CREATE USER 'databasewiz'@'localhost' IDENTIFIED BY 'database1';
CREATE USER 'love4coding'@'localhost' IDENTIFIED BY 'database22';
flush privileges;
-- NOTE: *must*  flush privileges after creating or removing users and/or privileges!

-- Check:
use mysql;
select * from user;

-- also, can drop users:
drop user if exists