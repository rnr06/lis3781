> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Rhianna Reichert

### Project 2 Requirements:

*Three Parts:*

1. Complete work in Mongo DB Shell Command
2. Import primer-dataset.json into bin directory
3. Chapter Questions
    - Chapter 16
    - MongoDB (NoSQL)

#### README.md file should include the following items:

* Screenshot(s) of Mongo DB Shell Command
* Screenshot of Optional JSON code for required reports
* Bitbucket repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Project Screenshots:

*Screenshots of Mongo DB Shell Commands*

|        P2 Solution #1        |       P2 Solution #3   |       P2 Solution #8   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![ P2 Solution #1](img/p2_solution_1_b.png " P2 Solution #1")              |                    ![ P2 Solution #3](img/p2_solution_3.png " P2 Solution #3")|                    ![ P2 Solution #8](img/p2_solution_8.png " P2 Solution #8")


|        P2 Solution #12        |      P2 Solution #16   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![ P2 Solution #12](img/p2_solution_12.png " P2 Solution #12")              |                    ![ P2 Solution #16](img/p2_solution_16.png " P2 Solution #16")|


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
