> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Rhianna Reichert

### Assignment 1 Requirements:

*Four Parts:*

1. Locally create rnr06 database using SQL
2. Locally create two tables using SQL
    - a.) company table
    - b.) customer table
3. Create two different users with different passwords
4. Chapter Questions
    - Chapter 11


### A1 Database Business Rules:

The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories must be tracked. Also, include the following business rules:

* Each employee may have one or more dependents.
* Each employee has only one job.
* Each job can be held by many employees
* Many employees may receive many benefits.
* Many benefits may be selected by many employees (though, while they may not select any benefits—any dependents of employees may be on an employee’s plan).

Notes:
* Employee/Dependent tables must use suitable attributes (See Assignment Guidelines)

In Addition:
* Employee: SSN, DOB, start/end dates, salary;
* Dependent: same information as their associated employee (though, not start/end dates), date added (as dependent), type of relationship: e.g., father, mother, etc.
* Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)
* Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)
* Plan: type (single, spouse, family), cost, election date (plans must be unique)
* Employee history: jobs, salaries, and benefit changes, as well as who made the change and why;
* Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ);
* *All* tables must include notes attribute.

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables
    * 1.) company table
    * 2.) customer table
* Bitbucket repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of A2 SQL Code*

![Screenshot of A1 ERD](img/A1_ERD.png "Screenshot of A1 ERD")

|        A2 SQL Code - 1        |       A2 SQL Code - 2   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![A2 SQL Code - 1](img/lis3781_a2_solutions_1.png "A2 SQL Code - 1")              |                    ![A2 SQL Code - 2](img/lis3781_a2_solutions_2.png "A2 SQL Code - 2")                  |

|        A2 SQL Code - 3        |                                     
|:---------------------------------:|
|              ![A2 SQL Code - 3](img/lis3781_a2_solutions_3.png "A2 SQL Code - 3")                 |


*Screenshot of A2 Populated Tables*

|        A2 Company Table        |       A2 Customer Table   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![A2 Company Table](img/company_table.png "A2 Company Table")              |                    ![A2 Customer Table](img/customer_table.png "A2 Customer Table")




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
