/*1. Create and populate tables
2. Log in using Oracle SQL Developer

Note: results to text F5, results to grid F9 (must select  SQL first!)

Note: SQL Developer *Single* Query result tab
https://stackoverflow.com/quesitons/2270951/sql-developer-single-query-result-tab-please
*/

--Note: Clear output window (remove appended output)
clear screen;

-- Also...
-- Ctrl+Shift+D - clears output window(though, must be in output window first!)
-- Note: Alt+PgDn - puts you ina Script Output panel. Ctrl+Shift+D - clears panel. Alt+PgUp - puts you back in editor panel.

-- General user and database reports:
-- Display  Oracle version:
-- 1.
SELECT * FROM PRODUCT_COMPONENT_VERSION;

-- 2. Retrieve all version information from Oracle:
SELECT * FROM V$VERSION;
-- Retrieve Oracle version only:
SELECT * FROM v$version WHERE banner LIKE 'Oracle%';

/*
DUAL is a table automatically created by Oracle Database along with the data dictionary.
DUAL is in the schema of the user SYS but is accessible by the name DUAL to all users.
It has one column, DUMMY, defined to be VARCHAR2(1), and contains one row with a value X.
https://www.oracletutorial.com/oracle-basics/oracle-dual-table/
https://docs.oracle.com/cd/B19306_01/server.102/b14200/queries009.htm
*/
-- Display current user:
--3 .
select user from dual;

-- Display  current day/time (formatted):
-- 4.
SELECT TO_CHAR
    (SYSDATE, 'MM-DD_YYYY HH12:MI:SS AM') "NOW"
    FROM DUAL;

-- Display  your privileges:
-- 5.
SELECT * FROM USER_SYS_PRIVS;
-- SELECT * FROM USER_TAB_PRIVS;
-- SELECT * FROM USER_ROLE_PRIVS;

-- Display all user tables:
-- 6.
SELECT OBJECT_NAME
    FROM USER_OBJECTS
WHERE OBJECT_TYPE = 'TABLE';

-- also...
SELECT table_name
FROM user_tables;

-- also... (note: user name is case-sensitive!)
SELECT owner, table_name
FROM all_tables
where owner='RNR';

-- Display structure for each table:
-- 7.
describe customer;
describe commodity;
describe "order";

-- Display data for each table:
select * from customer;
select * from commodity;
select * from "order";

-- 8. List the customer number, last name, first name, and e-email of every customer.
select cus_id, cus_lname, cus_fname, cus_email
from customer;

-- Note: Because you need to list all customers,
-- you do not need to include WHERE clause (in other words, there are no restrictions).

-- 9. Same query as above, include street, city, state, and sort by state in descending order, and last name in ascending order.
select cus_id, cus_lname, cus_fname, cus_street, cus_city, cus_state, cus_email
from customer
order by cus_state desc, cus_lname;

-- 10. What is the full name of customer number 3? Display last name first.
select cus_lname, cus_fname
from customer
where cus_id = '3';

/*
Note: You can use the WHERE clause to restrict the query output to customer number 3.
The condition in the preceding WHERE clause is called a simple condition.
A simple condition has the form: column namem comparison opertor, and then either another column name or a value.
Note that there are two different operators for not equal to (< > and !=).
*/

-- Practice:
-- a. Find the customer number for every customer whose last name is Taylor.
select cus_id
from customer
where cus_lname = 'Taylor';

-- b. Same as above using a case-insensitive search  (e.g., will find taylor, Taylor, tAYlor, etc.):
select cus_id
from customer
where lower(cus_lname) = 'taylor';

-- 11. Find the customer number, last name, first name, and current balance for every customer whose balance exceeds $1,000. sorted by largest to smallest balances.
select cus_id, cus_lname, cus_fname, cus_balance
from customer
where cus_balance > 1000
order by cus_balance desc;

-- 12. List the name of every commodity, and its price (formatted to two decimal places, displaying $ sign), sorted by smallest to largest price.
select com_name, to_char(com_price, 'L99,999.99') as price_formatted
from commodity
order by com_price;

-- 13. Display all customers' first and last names, streets, cities, states, and zip codes as follows (ordered by zip code descending).
select (cus_lname || ', ' || cus_fname) as name,
(cus_street || ', ' || cus_city || ', ' || cus_state || ' ' || cus_zip) as address
from customer
order by cus_zip desc;

-- 14. List all orders not including cereal--use subquery to find commodity id for cereal
select * from "order"
where com_id not in (select com_id from commodity where lower(com_name)='cereal');

select * from "order"
where com_id != (select com_id from commodity where lower(com_name)='cereal');

select * from "order"
where com_id <> (select com_id from commodity where lower(com_name)='cereal');

-- 15. List the customer number, last name, first name, and balance for every customer whose balance is between $500 and $1,000, (format currency to two decimal places, displaying $ sign).

-- left-aligns numbers
select cus_id, cus_lname, cus_fname, to_char(cus_balance, '$99,999.99') as balance_formatted
from customer
where cus_balance >= 500 and cus_balance <= 1000;

-- right-aligns numbers
select cus_id, cus_lname, cus_fname, to_char(cus_balance, 'L99,999.99') as balance_formatted
from customer
where cus_balance >= 500 and cus_balance <= 1000;

-- Or...
select cus_id, cus_lname, cus_fname, to_char(cus_balance, 'L99,999.99') as balance_formatted
from customer
where cus_balance between 500 and 1000;

-- 16. List the customer number, last name, first name, and balance for every customer whose balance is greater than the average balance, (format currency to two decimal places, displaying $ sign).
select cus_id, cus_lname, cus_fname, to_char(cus_balance, 'L99,999.99') as balance_formatted
from customer
where cus_balance > (select avg(cus_balance) from customer);

-- 17. List the customer number, name, and *total* order amount for each customer sorted in descending *total* order amount, (format currency to two decimal places, displaying $ sign), and include
-- an alias “total orders” for the derived attribute.
select cus_id, cus_lname, cus_fname, to_char(sum(ord_total_cost), 'L99,999.99') as "total orders"
from customer
    natural join "order"
group by cus_id, cus_lname, cus_fname
order by sum(ord_total_cost) desc;

-- 18. List the customer number, last name, first name, and complete address of every customer who lives on a street with "Peach" anywhere in the street name.
select cus_id, cus_lname, cus_fname, cus_street, cus_city, cus_state, cus_zip
from customer
where cus_street like '%Peach%';

-- 19. List the customer number, name, and *total* order amount for each customer whose *total* order amount is greater than $1500, for each customer sorted in
-- descending *total* order amount, (format currency to two decimal places, displaying $ sign), and include an alias “total orders” for the derived attribute.
select cus_id, cus_lname, cus_fname, to_char(sum(ord_total_cost), 'L99,999.99') as "total orders"
from customer
    natural join "order"
group by cus_id, cus_lname, cus_fname
having sum(ord_total_cost) > 1500
order by sum(ord_total_cost) desc;

-- 20. List the customer number, name, and number of units ordered for orders with 30, 40, or 50 units ordered.
select cus_id, cus_lname, cus_fname, ord_num_units
from customer
    natural join "order"
where ord_num_units IN (30, 40, 50);

-- 21. Using EXISTS operator: List customer number, name, number of orders, minimum, maximum, and sum of their
-- order total cost, only if there are 5 or more customers in the customer table, (format currency to two decimal places, displaying $ sign).
select
    cus_id, cus_lname, cus_fname,
    count(*) as "number of orders",
    to_char(min(ord_total_cost), 'L99,999.99') as "minimum order cost",
    to_char(max(ord_total_cost), 'L99,999.99') as "maximum order cost",
    to_char(sum(ord_total_cost), 'L99,999.99') as "total orders"
from customer
natural join "order"
where EXISTS
    (select count(*) from customer having COUNT(*) >= 5)
group by cus_id, cus_lname, cus_fname;

-- 22. Find aggregate values for customers: (Note, difference between count(*) and count(cus_balance), one customer does not have a balance.)
select
    count(*),
    count(cus_balance),
    sum(cus_balance),
    avg(cus_balance),
    max(cus_balance),
    min(cus_balance)
from customer;

-- 23. Find the number of unique customers who have orders.
-- list all customer numbers including duplicates
select cus_id from "order";

-- list all customer numbers excluding duplicates
select distinct cus_id from "order";

-- list number of unique customers
select distinct count(distinct cus_id) from "order";

-- 24. List the customer number, name, commodity name, order number, and order amount for each customer order, sorted in descending order amount,
-- (format currency to two decimal places, displaying $ sign), and include an alias “order amount” for the derived attribute.
select cu.cus_id, cus_lname, cus_fname, com_name, ord_id, to_char(ord_total_cost, 'L99,999.99') as "order amount"
from customer cu
    join "order" o on o.cus_id=cu.cus_id
    join commodity co on co.com_id=o.com_id
order by ord_total_cost desc;

-- 25. Modify prices for DVD players to $99. Note: First, *be sure* to SET DEFINE OFF (don't use a semi-colon on the end).
SET DEFINE OFF

UPDATE commodity
SET com_price = 99
WHERE com_name = 'DVD & Player';