> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Rhianna Reichert

### Assignment 4 Requirements:

*Three Parts:*

1. Create Oracle Server connection
2. Create three tables in Oracle using SQL
    * a.) customer table
    * b.) commodity table
    * c.) order table
3. Chapter Questions
    - Chapter 14

#### README.md file should include the following items:

* Screenshot of Oracle SQL code
* Screenshot of populated tables
    * 1.) customer table
    * 2.) commodity table
    * 3.) order table
* Bitbucket repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of A3 Populated Oracle Tables*

|        A3 Customer Table        |       A3 Commodity Table   |       A3 Order Table   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![A3 Customer Table](img/customer_table.png "A3 Customer Table")              |                    ![A3 Commodity Table](img/commodity_table.png "A3 Commodity Table")|                    ![A3 Order Table](img/order_table.png "A3 Order Table")


*Screenshot of A3 Oracle SQL Code*

|        A3 SQL Code - 1        |       A3 SQL Code - 2   |       A3 SQL Code - 3   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![A3 SQL Code - 1](img/a3_code_1.png "A3 SQL Code - 1")              |                    ![A3 SQL Code - 2](img/a3_code_2.png "A3 SQL Code - 2")                  |                    ![A3 SQL Code - 3](img/a3_code_3.png "A3 SQL Code - 3")                  


*Screenshot of A3 Oracle SQL Solutions Code*

|        A3 SQL Solutions Code - 1        |        A3 SQL Solutions Code - 2        |          A3 SQL Solutions Code - 3        |                                      
|:---------------------------------:|:---------------------------------:|:---------------------------------:|
|              ![A3 SQL Solutions Code - 1](img/solution_21_code.png "A3 SQL Solutions Code - 1")                 |              ![A3 SQL Solutions Code - 2](img/solution_22_code.png "A3 SQL Solutions Code - 2")                 |              ![A3 SQL Solutions Code - 3](img/solution_25_code.png "A3 SQL Solutions Code - 3")    


*Screenshot of A3 Oracle Outcomes*

|        A3 SQL Outcome - 1        |        A3 SQL Outcome - 2        |           A3 SQL Outcome - 3       |                                      
|:---------------------------------:|:---------------------------------:|:---------------------------------:|
|              ![A3 SQL Outcome - 1](img/solution_12.png "A3 SQL Outcome - 1")                 |              ![A3 SQL Outcome - 2](img/solution_17.png "A3 SQL Outcome - 2")                 |              ![A3 SQL Outcome - 3](img/solution_24.png "A3 SQL Outcome - 3")   

|        A3 SQL Outcomes - ALL       |                                  
|:---------------------------------:|
|              ![A3 SQL Outcomes - ALL](img/a3_outcomes.gif "A3 SQL Outcomes - ALL")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
