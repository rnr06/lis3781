> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Rhianna Reichert

### Assignment 4 Requirements:

*Two Parts:*

1. Create database in MS SQL Server
2. Chapter Questions
    - Chapter 13

#### README.md file should include the following items:

* Screenshot of MS SQL code
* Screenshot of populated ERD
* Bitbucket repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of A4 ERD*

|        A4 ERD        |                                   
|:---------------------------------:|
|              ![A4 ERD](img/a4_erd.png "A4 ERD")              |


*Screenshot of A4 SQL Solutions*

|        A4 Solution #1        |       A4 Solution #2   |       A4 Solution #3   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![A4 Solution #1](img/solution_1.png "A4 Solution #1")              |                    ![A4 Solution #2](img/solution_2.png "A4 Solution #2")|                    ![A4 Solution #3](img/solution_3.png "A4 Solution #3")


|        A4 Solution #4        |      A4 Solution #5   |       A4 Extra Credit Solution   |                                       
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![A4 Solution #4](img/solution_4.png "A4 Solution #4")              |                    ![A4 Solution #5](img/solution_5.png "A4 Solution #5")|                    ![A4 Extra Credit Solution](img/extra_credit_solution.png "A4 Extra Credit Solution")             


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
